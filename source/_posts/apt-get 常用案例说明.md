---
title: Apt-get 常用示例
comments: true
tags: apt-get
categories: linux
keywords: 'apt-get,linux'
abbrlink: 578d7cdf
description:
---

是Debian及其派生的Linux软件包管理器。在Debian系统中，APT建构于dpkg之上。APT的软件安装来源在Debian安装的时候即可进行初始设置，除了Debian官方的网络安装来源之外，也可以使用Debian的安装光盘，甚至可以从非官方的安装来源中下载非官方的软件。APT同时也可以从一些安装来源中下载源代码软件，并且自行编译、安装。

<!--more-->

注意：'xyz'只是一个占位符。

## 系统更新
- `apt-get update` 下载新的软件包列表。
- `apt-get upgrade` 安装您拥有的软件包的较新版本。
- `apt-get dist-upgrade` 在先前的命令中执行升级，并智能地处理新版本软件包的依赖关系，并在必要时尝试以不重要的软​​件包为代价来升级最重要的软件包；有时在极少数必要的情况下删除软件包。

## 软件包搜索和软件包信息
- `apt-cache pkgnames` 列出所有可用的软件包。
- `apt-cache xyz` 搜索软件包。
- `apt-cache search xyz` 搜索包含说明的结果的软件包。
- `apt-cache pkgnames xyz` 列出所有以“ xyz”开头的软件包。
- `apt-cache show xyz` 检查包装信息。
- `apt-cache showpkg xyz` 检查软件包的依赖关系。
- `apt-cache stats` 显示有关缓存的整体统计信息。

## 软件包安装和删除
- `apt-get download xyz` 仅将软件包下载到当前工作目录。
- `apt-get install xyz` 下载并安装软件包。通过在第一个软件包之后拥有所需的多个软件包来安装。
- `apt-get install xyz --no-upgrade` 将软件包标记为永不升级。
- `apt-get install xyz --only-upgrade` 仅升级指定的软件包，并禁用软件包的新安装。
- `apt-get install '*name*'` 使用*通配符将安装几个包含'* name *'字符串的软件包，如软件包名称中所示。
- `apt-get changelog xyz` 下载软件包更改日志并显示已安装的软件包版本。
- `apt-get check` 检查损坏的依赖关系。
- `apt-get remove xyz` 卸载软件包。
- `apt-get purge xyz` 删除软件包中剩余的配置文件。
- `apt-get autoremove` 删除孤立的软件包。
- `apt-get clean` 删除下载的deb文件。
- `apt-get autoclean` 删除已下载的无法再下载的deb文件。

## 源代码包
- `apt-get source xyz` 将包的源代码下载并解压缩到目录中。
- `apt-get build-dep xyz` 下载并安装软件包的构建依赖项。
- `apt-get --compile source xyz` 下载，解压缩和编译源代码。