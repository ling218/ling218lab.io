---
title: Vim 语法高亮并显示行号
comments: true
tags: vim
categories: linux
keywords: 'linux,vim'
abbrlink: 2c4db044
description: 整理记录
---


## 临时生效：

```
:syntax on  #高亮开
:syntax off  #高亮关
:set nu  #行号
```

## 永久生效：

切换到当前登陆用户家目录并编辑 .vimrc 文件。

```
# cd ~
# vim .vimrc
```

添加以下行：

```
syntax on
set nu
```

