---
title: 将自定义 Debian 安装到硬盘
comments: true
tags: live-build
categories: live-build
keywords: debian live build
description: 将自定义Debian安装到硬盘
abbrlink: df9fb8e4
---


Debian提供的库存Debian安装程序非常棒。它直观，简单。但是，如果出于某种原因（特定配置，计算机部署，最小映像）想要自定义安装，则可以使用以下步骤来完成。

为此，我们将需要任何基于Debian的实时CD。您可以使用我的指南来[创建自定义的Debian Live CD或USB](https://willhaley.com/blog/custom-debian-live-environment/)来生成该文件，也可以仅使用Ubuntu或Debian Live CD或USB。

使用自定义实时环境的优势在于您可以将所需工具打包在实时环境中，以便它们始终可用。如果您使用库存的Ubuntu或Debian Live CD，则可能需要网络连接才能安装所需的工具。

**警告**：我已**突出显示**您应该在目标安装chroot中的所有位置。

1. 使用实时CD或USB引导计算机并安装必要的工具（假设它们尚不可用）。

   ```
   sudo apt-get install debootstrap
   ```

2. 确定要在其中安装自定义Debian的设备。

   我假设您打算使用的硬盘位于`/dev/sdz`，您只希望该磁盘上有一个分区，并且希望将MBR与grub2一起使用。

   您**必须**替换的任何实例`/dev/sdz`与同位置在这些步骤中**您的**硬盘驱动器或会不小心丢失数据。

3. 用一个可引导的Linux分区创建一个MBR表。

   ```
   echo -e "o\nn\np\n1\n\n\nw" | sudo fdisk /dev/sdz
   ```

   ```
   echo -e "a\nw\n" | sudo fdisk /dev/sdz
   ```

4. 格式化分区。

   ```
   sudo mkfs.ext4 /dev/sdz1
   ```

5. 如果安装点尚不存在，请创建一个。

   ```
   sudo mkdir -p /mnt
   ```

6. 挂载分区。

   ```
   sudo mount /dev/sdz1 /mnt
   ```

7. 设置基本的Debian安装。我在我的发行版中使用Stretch，在架构上使用i386。如果您不在美国，或者您知道更近的镜子，请更换镜子。

   ```
   sudo debootstrap \
       --arch=i386 \
       --variant=minbase \
       stretch /mnt http://ftp.us.debian.org/debian/
   ```

8. 绑定`/dev`并`/proc`从主机到chroot。

   ```
   sudo mount -o bind /dev /mnt/dev
   ```

   ```
   sudo mount -t proc /proc /mnt/proc
   ```

9. Chroot到我们的Debian安装。

   ```
   sudo chroot /mnt
   ```

10. **chroot**

    找出要在安装中使用的Linux内核。

    ```
    apt-cache search linux-image
    ```

    我选择了图像`linux-image-586`。`systemd-sys`（或等效项）是必须提供的`init`。

    ```
    apt-get update && \
    apt-get install --no-install-recommends \
        linux-image-586 systemd-sysv
    ```

11. **chroot**

    安装您选择的程序。我使用`--no-install-recommends`以避免多余的程序包。您应该确定安装所需的内容。

    ```
    apt-get install --no-install-recommends \
        network-manager net-tools wireless-tools wpagui \
        tcpdump wget openssh-client \
        blackbox xserver-xorg-core xserver-xorg x11-xserver-utils \
        xinit xterm \
        pciutils usbutils gparted nano
    ```

12. **chroot**

    创建`/etc/fstab`用于安装的文件。

    我们可以使用类似的脚本来生成它。

    ```
    UUID=`blkid -s UUID -o value /dev/sdz1`
    ```

    ```
    echo "UUID=${UUID} / ext4 defaults 1 1" > /etc/fstab
    ```

13. **chroot**

    安装grub引导加载程序。

    ```
    apt-get install grub2
    ```

    出现提示时，请确保选择`/dev/sdz`（**不是** `/dev/sdz1`）作为`Grub install device`。

14. **chroot**

    设置root密码。

    ```
    passwd root
    ```

15. **chroot**

    ```
    exit
    ```

16. 重新启动您的Debian安装！

## 原文地址：

- https://willhaley.com/blog/custom-debian-hard-drive-install/