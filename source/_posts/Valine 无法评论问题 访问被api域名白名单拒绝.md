---
title: Valine 无法评论问题 访问被api域名白名单拒绝
comments: true
tags: Valine
categories: blog
keywords: 'blog,Valine'
description: 'Valine 出现 Code 403: 访问被api域名白名单拒绝，请检查你的安全域名设置. 解决办法'
abbrlink: 7b2bb70c
---


确保**主题配置文件_config.yml** Valine 配置正确的条件下。

我的问题是 Github pages HTTPS证书过期。

在leancloud的应用>设置>安全中心>Web安全域名中只添加了带HTTPS证书的域名。

带上http证书的域名就解决问题了。但是却让我查找了很久的问题。

Web安全域名 例：
```
https://www.ling218.cn
https://ling218.github.io
http://www.ling218.cn
http://ling218.github.io
```
