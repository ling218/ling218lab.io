---
title: 官方：在基于 Debian 的系统上构建 Kali ISO
comments: true
tags: live-build
categories: live-build
keywords: 'live-build,kali,linux,debian'
description: 可以在Kali以外的基于Debian的系统上轻松运行实时构建
abbrlink: 9308cb62
---


您可以在Kali以外的基于Debian的系统上轻松运行实时构建。以下说明已经过测试，可以与Debian和Ubuntu一起使用。

首先，我们通过确保已完全更新来准备系统，然后继续下载Kali存档密钥环和实时构建软件包。

```
sudo apt update
sudo apt upgrade
cd /root/

wget http://http.kali.org/pool/main/k/kali-archive-keyring/kali-archive-keyring_2018.2_all.deb
wget https://archive.kali.org/kali/pool/main/l/live-build/live-build_20190311_all.deb
```

完成后，我们将安装一些其他依赖项和以前下载的文件。

```
sudo apt install -y git live-build cdebootstrap debootstrap curl
sudo dpkg -i kali-archive-keyring_2018.2_all.deb
sudo dpkg -i live-build_20190311_all.deb
```

准备好所有环境后，我们通过设置构建脚本并签出构建配置来开始实时构建过程。

```
cd /usr/share/debootstrap/scripts/
echo "default_mirror http://http.kali.org/kali"; sed -e "s/debian-archive-keyring.gpg/kali-archive-keyring.gpg/g" sid > /tmp/kali
sudo mv /tmp/kali .
sudo ln -s kali kali-rolling

cd ~
git clone https://gitlab.com/kalilinux/build-scripts/live-build-config.git

cd live-build-config/
```

此时，我们必须编辑`build.sh`脚本以绕过版本检查。为此，我们注释掉下面的“出口1”。

```
# Check we have a good debootstrap
ver_debootstrap=$(dpkg-query -f '${Version}' -W debootstrap)
if dpkg --compare-versions "$ver_debootstrap" lt "1.0.97"; then
if ! echo "$ver_debootstrap" | grep -q kali; then
echo "ERROR: You need debootstrap >= 1.0.97 (or a Kali patched debootstrap). Your current version: $ver_debootstrap" >&2
exit 1
fi
fi
```

进行更改后，脚本应如下所示：

```
# Check we have a good debootstrap
ver_debootstrap=$(dpkg-query -f '${Version}' -W debootstrap)
if dpkg --compare-versions "$ver_debootstrap" lt "1.0.97"; then
if ! echo "$ver_debootstrap" | grep -q kali; then
echo "ERROR: You need debootstrap >= 1.0.97 (or a Kali patched debootstrap). Your current version: $ver_debootstrap" >&2
# exit 1
fi
fi
```

此时，我们可以正常建立ISO

```
sudo ./build.sh --variant light --verbose
```

