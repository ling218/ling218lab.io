---
title: Debian系统下 rpm 和 deb 包相互转换
comments: true
tags: 'rpm,deb'
categories: linux
keywords: 'linux,rpm,deb'
description: Debian rpm和deb包相互转换
abbrlink: a722ca9a
---


## 安装alien软件包：

```
# apt update
# apt install alien -y
```



## RPM=>DEB

```
alien google-chrome.rpm google-chrome.deb generated
```



## DEB=>RPM(添加参数-r进行转换)

```
alien -r google-chrome.deb google-chrome.rpm generated
```



## DEB包手动安装

```
dpkg -i google-chrome.deb
```

