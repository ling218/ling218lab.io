---
title: Linux 压缩和解压.bz2文件
comments: true
tags: bz2
categories: linux
keywords: 'linux,ba2'
description: Debain 压缩和解压.bz2文件
abbrlink: a6bd2dda
---



1、如果没有安装，执行如下命令安装。

```
# apt update
# apt install bzip2
```

2、使用bzip2压缩文件。

```
# bzip2 filename/ 
# bzip2 -z filename 
```

上面命令执行后，会把原文件删除，如果要保留原文件，使用-k选项。

```
# bzip2 -zk filename
```

-f 选项强制覆盖已经存在的文件。

3、使用bzip2解压文件。(-d 选项解压)

```
# bzip2 -d filename.bz2
```

```
# bzip2 -vfd filename.bz2
```

4、选项总结。

-  -f 覆盖文件 
- -v 可视化输出 
- -k 保留原文件
- -d 解压文件