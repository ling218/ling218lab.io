---
title: 项目：bash 脚本轻松生成实时 Linux ISO 镜像
comments: true
tags: linux
categories: linux
keywords: 'linux,build iso'
description: 一个脚本可以轻松生成功能齐全的实时Linux ISO映像
abbrlink: b73d248a
---



一个脚本可以轻松生成功能齐全的实时Linux ISO映像。这是基于[Minimal Linux Live](http://github.com/ivandavidov/minimal)的第一个发布版本，并在下一版本中进行了一些改进。已删除所有空行和注释，并且已修改脚本以减少总长度。

下面的脚本使用**Linux内核4.19.12**，**BusyBox 1.29.3**和**Syslinux 6.03**。源包将自动下载和编译。如果您使用的是Ubuntu或Linux Mint，则应该能够通过执行以下命令来解决所有构建依赖项：

```
# sudo apt install wget make gawk gcc bc bison flex xorriso libelf-dev libssl-dev
```

之后，只需运行以下脚本。它不需要 **root** 权限。最后，生成**minimal_linux_live.iso**文件，在执行脚本的同一目录中。

```
 wget http://mirrors.ustc.edu.cn/kernel.org/linux/kernel/v4.x/linux-4.19.12.tar.xz
 wget http://busybox.net/downloads/busybox-1.29.3.tar.bz2
 wget http://mirrors.ustc.edu.cn/kernel.org/linux/utils/boot/syslinux/syslinux-6.03.tar.xz
 mkdir isoimage
 tar -xvf linux-4.19.12.tar.xz
 tar -xvf busybox-1.29.3.tar.bz2
 tar -xvf syslinux-6.03.tar.xz
 cd busybox-1.29.3
 make distclean defconfig
 sed -i "s|.*CONFIG_STATIC.*|CONFIG_STATIC=y|" .config
 make busybox install
 cd _install
 rm -f linuxrc
 mkdir dev proc sys
 echo '#!/bin/sh' > init
 echo 'dmesg -n 1' >> init
 echo 'mount -t devtmpfs none /dev' >> init
 echo 'mount -t proc none /proc' >> init
 echo 'mount -t sysfs none /sys' >> init
 echo 'setsid cttyhack /bin/sh' >> init
 chmod +x init
 find . | cpio -R root:root -H newc -o | gzip > ../../isoimage/rootfs.gz
 cd ../../linux-4.19.12
 make mrproper defconfig bzImage
 cp arch/x86/boot/bzImage ../isoimage/kernel.gz
 cd ../isoimage
 cp ../syslinux-6.03/bios/core/isolinux.bin .
 cp ../syslinux-6.03/bios/com32/elflink/ldlinux/ldlinux.c32 .
 echo 'default kernel.gz initrd=rootfs.gz' > ./isolinux.cfg
 xorriso \
   -as mkisofs \
   -o ../minimal_linux_live.iso \
   -b isolinux.bin \
   -c boot.cat \
   -no-emul-boot \
   -boot-load-size 4 \
   -boot-info-table \
   ./
 cd ..
```

请注意，此脚本生成非常小的实时Linux操作系统，仅支持shell，不支持网络。网络功能已在[Minimal Linux Live](http://github.com/ivandavidov/minimal)项目中正确实现，该项目具有广泛的文档记录和更丰富的功能，但仍然可以生成非常小的实时Linux ISO映像。