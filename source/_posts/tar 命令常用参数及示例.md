---
title: Tar 命令常用参数及示例
comments: true
tags: tar
categories: linux
keywords: 'linux,tar'
abbrlink: 91af96f
description:
---

tar 命令常用来把文件和目录打包成一个文件。

<!--more-->



## 选项参数：

- -c 创建压缩文件
- -x 解压文件
- -v 显示进度
- -f 文件名
- -t 查看压缩文件内容
- -j 通过bzip归档
- -z 通过gzip归档
- -r 在压缩文件中追加文件或目录 
- -W 验证压缩文件



## 示例：

### 把目录/home/abc/code 打包为code.tar

```
# tar -cvf code.tar /home/abc/code/
```



### 压缩为tar.gz 格式的包

```
# tar cvzf code.tar.gz /home/abc/code/
```



### 压缩率更高的tar.bz2 格式的包

```
# tar cvfj code.tar.bz2 /home/abc/code/
```



### 解压tar包

```
# tar -xvf code.tar -C /home/abc/code
```



### 解压tar.gz包

```
# tar -xvf code.tar.gz
```



### 解压tar.bz2 包

```
# tar -xvf code.tar.bz2
```



### 列出tar包内容 

```
# tar -tvf code.tar
```



### 解压tar包中的单个文件

```
# tar --extract --file=code.tar Readme.txt
```



### 解压tar包中的多个文件

```
# tar -xvf code.tar "file1" "file2"
```



### 解压同一种类型的文件（下面是解压txt文件）

```
# tar -xvf code.tar --wildcards *.txt'
```



### 在tar包中加入文件或目录

```
# tar -rvf code.tar abcd.txt
# tar -rvf code.tar Doc/
```



### tar帮助信息查看

```
# man tar
```

