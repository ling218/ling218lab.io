---
title: 查询Linux发行版名称与版本
comments: true
tags: linux
categories: linux
keywords: linux
description: 怎么查看Linux系统的发行版和版本?
abbrlink: ab7d1113
---



在Linux系统下查询发行版名称和版本有许多种方法,而不同的Linux发行版查询的方式也不所同,以下整理了一些常见的方法。

## 查询 Linux 发行版与版本

Linux 发行版与版本就是指大家常听到的 Ubuntu 16.04、Fedora 24、CentOS 7 这些名字,而要查询 Linux 的发行版有以下几中方式。

## 查看 /etc/*-release 文件

通常在/etc目录中会有几个*-release文件,从这里面的内容可以看出Linux的发行版名称与版本号。
```
cat /etc/redhat-release
```
```
cat /etc/os-release

NAME="Ubuntu"
VERSION="16.04.1 LTS (Xenial Xerus)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 16.04.1 LTS"
VERSION_ID="16.04"
HOME_URL="http://www.ubuntu.com/"
SUPPORT_URL="http://help.ubuntu.com/"
BUG_REPORT_URL="http://bugs.launchpad.net/ubuntu/"
UBUNTU_CODENAME=xenial
```
```
cat /etc/lsb-release

DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=16.04
DISTRIB_CODENAME=xenial
DISTRIB_DESCRIPTION="Ubuntu 16.04.1 LTS"
```
## 使用 lsb_release 命令

lsb_release是一个用来查询 Linux 发行版信息的命令,但是并不是每一种 Linux 发行版都会预装这个指令:
```
lsb_release -a

No LSB modules are available.
Distributor ID: Ubuntu
Description:Ubuntu 16.04.1 LTS
Release:16.04
Codename:xenial
```
## 查询 Linux 内核版本

uname是一个用来查询 Linux 系统内核版本的命令:
```
uname -a

Linux snail 4.4.0-31-generic #50-Ubuntu SMP Wed Jul 13 00:07:12 UTC 2016 x8
```

## 查看 /proc/version

在 /proc/version 中也会纪录一些 Linux 的内核版本信息:
```
cat /proc/version

Linux version 4.4.0-31-generic (buildd@lgw01-16) (gcc version 5.3.1 2016041
```