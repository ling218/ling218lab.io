---
title: 防止远程 Linux 主机自动断开 SSH 连接
comments: true
tags: ssh
categories: linux
keywords: 'ssh,linux'
description: ssh终端连接防自动断开
abbrlink: e41226c8
---


在使用ssh连接远程Linux主机时，如果长时间不操作，ssh会自动断开，只能重新登陆。

> 由于ssh的安全机制，如果10分钟没有任何操作，本次SSH会话会自动关闭。

## 怎么防止远程Linux自动断开SSH连接

下面的操作是在本地ssh客户端上，不是远程主机。

编辑SSH配置文件：

```
vim ~/.ssh/config   #当前登录用户生效
```

添加：

```
Host *
  ServerAliverInterval 30
```

*号代表所有主机，你可以指定某个主机，如：

```
Host server1
  ServerAliverInterval 30
```

ServerAliveInterval 30表示ssh客户端每隔30秒给远程主机发送一个no-op包，no-op是无任何操作的意思，这样远程主机就不会关闭这个SSH会话。

为了使所有用户生效，你可以在/etc/ssh/ssh_config全局配置文件添加如下一行：

```
ServerAliveInterval 30
```

还可以在连接时使用选项：

```
ssh -o ServerAliveInterval=30 user@remote-ssh-server-ip
```
