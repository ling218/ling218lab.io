---
title: 自定义 Ubuntu Server 16.04.2 安装 iso
comments: true
tags: ubuntu
categories: linux
keywords: linux
description: 自定义安装 Ubuntu Server
abbrlink: cf3baf5f
---



## 下载 Ubuntu Server 16.04.2 ISO
```
 wget http://debian.linux.org.tw/ubuntu-releases/16.04.2/ubuntu-16.04.2-server-amd64.iso**
```

## 挂载光盘
```
mount -o loop ubuntu-16.04.2-server-amd64.iso /mnt
```

## 复制目录
```
mkdir ~/Ubuntu16042
cp -a /mnt/* ~/Ubuntu16042
cp -r /mnt/.disk ~/Ubuntu16042
```

## 修改 ~/Ubuntu16042/isolinux/txt.cfg
```
vim ~/Ubuntu16042/isolinux/txt.cfg

default ainstall
label ainstall
 menu label ^Auto Install Ubuntu Server
 kernel /install/vmlinuz
 append file=/cdrom/preseed/aubuntu-server.seed vga=788 initrd=/install/initrd.gz quiet debian-installer/language=en debian-installer/country=TW console-setup/ask_detect=false keyboard-configuration/layoutcode=us debian-installer/locale=en_US ---
label install
 menu label ^Install Ubuntu Server
 kernel /install/vmlinuz
 append file=/cdrom/preseed/ubuntu-server.seed vga=788 initrd=/install/initrd.gz quiet ---
```

## 建立 ~/Ubuntu16042/preseed/aubuntu-server.seed

### 准备安装 LAMP / OpenSSH Server
```
vim ~/Ubuntu16042/preseed/aubuntu-server.seed

d-i debian-installer/locale string en_US.UTF-8
d-i debian-installer/locale string en_US
d-i console-setup/ask_detect boolean false
d-i console-setup/layout string USA
d-i debian-installer/language string en
d-i debian-installer/country string TW
d-i console-setup/ask_detect boolean false
d-i keyboard-configuration/xkb-keymap select us 
Default user, change
d-i passwd/root-login boolean true
d-i passwd/make-user boolean false
d-i user-setup/encrypt-home boolean false
d-i user-setup/allow-password-weak boolean true
d-i netcfg/choose_interface select auto
d-i netcfg/get_hostname string unassigned-hostname
d-i netcfg/get_domain string unassigned-domain
d-i mirror/country string manual
d-i mirror/http/hostname string http://tw.archive.ubuntu.com
d-i mirror/http/hostname string http://free.nchc.org.tw
d-i mirror/http/directory string /ubuntu
d-i apt-setup/services-select multiselect security
d-i apt-setup/security_host string free.nchc.org.tw
d-i apt-setup/security_path string /ubuntu
d-i mirror/http/proxy string
d-i clock-setup/utc boolean false
d-i clock-setup/ntp boolean true
d-i time/zone string Asia/Taipei
d-i partman/confirm boolean true
d-i partman/choose_partition select finish
d-i partman/confirm_nooverwrite boolean true
d-i partman-auto/disk string /dev/[sv]da
d-i partman-auto/method string lvm
d-i partman-auto/choose_recipe select atomic
d-i partman-lvm/device_remove_lvm boolean true
d-i partman-lvm/confirm boolean true
d-i partman-lvm/confirm_nooverwrite boolean true
d-i partman-auto-lvm/guided_size string max
d-i partman-partitioning/confirm_write_new_label boolean true
d-i grub-installer/grub2_instead_of_grub_legacy boolean true
d-i grub-installer/only_debian boolean true
d-i grub-installer/bootdev string /dev/[sv]da
d-i pkgsel/update-policy select none
d-i pkgsel/include string unity ubuntu-desktop openssh-server
tasksel tasksel/first multiselect lamp-server, openssh-server
d-i pkgsel/include string openssh-server ntp build-essential rsync less vim ethtool curl lsscsi dstat htop strace
d-i finish-install/reboot_in_progress note
```

## 制作成 ISO 文件
```
cd ~/Ubuntu16042
mkisofs -r -V "Ubuntu-Server 16.04.2" -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul -boot-load-size 4 -boot-info-table -o ~/ubuntu-16.04.2-server-amd6_0501.iso .
```

## 安装测试

省略

## 参考网站：

[ubuntu-autoinstaller/txt.cfg at master · express42/ubuntu-autoinstaller · GitHub](https://github.com/express42/ubuntu-autoinstaller/blob/master/txt.cfg)

[InstallCDCustomization - Community Help Wiki](https://help.ubuntu.com/community/InstallCDCustomization)

[Ubuntu 16.04 / Debian 8: Run PXE boot server for automated install - Narrow Escape](https://www.hiroom2.com/2016/05/19/ubuntu-16-04-debian-8-run-pxe-boot-server-for-automated-install/)

[B.4. Contents of the preconfiguration file (for xenial)](https://help.ubuntu.com/16.04/installation-guide/i386/apbs04.html)
https://help.ubuntu.com/16.04/installation-guide/example-preseed.txt

[system installation - How do I create a completely unattended install of Ubuntu? - Ask Ubuntu](https://askubuntu.com/questions/122505/how-do-i-create-a-completely-unattended-install-of-ubuntu)
