---
title: Sytemctl 命令管理 systemd 服务
comments: true
tags: systemctl
categories: linux
keywords: 'linux,systemctl'
description: Sytemctl 命令管理 systemd 服务
abbrlink: 47790c48
---


```
systemctl start service  #启动服务
systemctl stop service  #停止服务
systemctl restart service  #重启服务
systemctl reload service  #重新加载配置文件
systemctl enable service  #开机启动
systemctl disable service  #关闭开机启动
systemctl status service  #服务状态
systemctl list-units  #列出系统中所有可用的服务
systemctl is-active application.service  #服务是否运行
systemctl is-enabled application.service  #服务是否开机启动
systemctl is-failed application.service  #服务运行是否正常
man systemctl  #帮助命令
```

