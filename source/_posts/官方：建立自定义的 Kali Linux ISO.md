---
title: 官方：建立自定义的 Kali Linux ISO
comments: true
tags: live-build
categories: live-build
keywords: 'live-build,kali,linux,ISO'
abbrlink: 796c51f8
description:
---

Kali Linux最强大的功能之一就是能够创建自己的包含自定义工具，桌面管理器和服务的发行版本。该研讨会将向您展示如何创建自己的个性化Kali Linux ISO，如何使用实时构建实用程序自定义各个方面，以及如何有效利用Kali中提供的各种元数据包。

<!--more-->



## 现场制作的出色

**0x00-首先更新存储库，安装必备组件，**然后从Kali Git存储库中签出live-build-config的新版本：

```
apt update
apt install git live-build cdebootstrap devscripts -y
git clone git://gitlab.com/kalilinux/build-scripts/live-build-config.git
cd live-build-config
```

**0x01-覆盖默认的Kali软件包列表**，仅包括所需的软件包。在视频中，我们仅编辑了列表并更改了一些软件包名称。

```
cat << EOF > kali-config/variant-default/package-lists/kali.list.chroot
kali-root-login
kali-defaults
kali-menu
kali-debtags
kali-archive-keyring
debian-installer-launcher
alsa-tools
locales-all
dconf-tools
openssh-server
EOF
```

**0x02-添加定制的syslinux引导条目**，其中包括用于定制预置文件的引导参数。

```
cat << EOF > kali-config/common/includes.binary/isolinux/install.cfg
label install
    menu label ^Install Automated
    linux /install/vmlinuz
    initrd /install/initrd.gz
    append vga=788 -- quiet file=/cdrom/install/preseed.cfg locale=en_US keymap=us hostname=kali domain=local.lan
EOF
```

**0x03-自定义ISO版本。**在此示例中，默认情况下我们将启动SSH服务。为此，我们可以使用chroot钩子脚本，该脚本位于“ hooks”目录中：

```
echo 'systemctl enable ssh' >>  kali-config/common/hooks/01-start-ssh.chroot
chmod +x kali-config/common/hooks/01-start-ssh.chroot
```

**0x04-接下来，我们下载壁纸**并将其覆盖。通知chroot的重叠文件是如何放置在*includes.chroot*目录。

```
mkdir -p kali-config/common/includes.chroot/usr/share/wallpapers/kali/contents/images
wget https://www.kali.org/dojo/blackhat-2015/wp-blue.png
mv wp-blue.png kali-config/common/includes.chroot/usr/share/wallpapers/kali/contents/images
```

**0x05-添加一个预置文件**，该**文件**将通过默认的Kali安装运行，无输入（无人值守）。我们可以包括现成的预置配置，并根据需要进行更改：

```
mkdir -p kali-config/common/debian-installer
wget https://gitlab.com/kalilinux/recipes/kali-preseed-examples/blob/master/kali-linux-full-unattended.preseed -O kali-config/common/debian-installer/preseed.cfg
```

**0x06-让我们将Nessus Debian软件包**包含在*packages*目录中，以包含在我们的最终版本中。由于我们使用的是64位版本，因此包含了64位Nessus Debian软件包。[下载](http://www.tenable.com/products/nessus/select-your-operating-system) Nessus .deb文件并将其放置在packages.chroot目录中：

```
mkdir kali-config/common/packages.chroot
mv Nessus-*amd64.deb kali-config/common/packages.chroot/
```

**0x07-现在您可以继续构建ISO了**，此过程可能需要一段时间，具体取决于您的硬件和Internet速度。完成后，可以在实时构建的根目录中找到您的ISO。

```
./build.sh -v
```

