---
title: 如何：自定义您的 Ubuntu Live CD
comments: true
tags: ubuntu
categories: live-build
keywords: 'ubuntu,livecd'
description: 自定义Ubuntu Live CD
abbrlink: 7138f05c
---


Live CD 非常棒，它们使您无需安装即可试用发行版。它们使您可以在任何计算机上运行自己喜欢的发行版，除此之外，它们还可以方便地恢复系统。

Ubuntu live CD已经打包了一些漂亮的软件，可以让您使用Live CD进行几乎所有的事情，但是仍然，它们可能是一些您不需要的软件，或者其中包含一些软件，不见了。

另一个为此的专业人士是，通过自定义图像，您将能够安装

本教程将显示一些步骤，以通过删除一些组件并添加一些其他组件来根据需要自定义Ubuntu Live CD。

在本教程中，我们将按照以下规范重新制作Ubuntu Gutsy Gibbon 7.10 Live CD：

- 删除非英语语言包
- 更新实时CD中随附的软件版本
- 启用Universe和Multiverse存储库
- 包括divx，mp3支持，realplayer…。
- 包括ndiswrapper支持
- 添加Firefox Flash-nonfree插件，添加Skype。
- 添加一些网络故障排除工具：traceroute，wireshark，kismet…

生成的图像将大于800M，因此它不适合CD，但可以适合DVD。

## 准备主机

首先，我们需要获取当前的Gutsy Gibbon Live CD映像并将其存储在〜/ Desktop上，而且，我们将需要安装额外的软件来重建我们的gutsy live CD squashfs：

```
$ sudo apt-get install squashfs-tools chroot
```

现在让我们开始设置工作环境。首先，我们将iso安装在/ tmp / livecd下：

```
$ mkdir /tmp/livecd
$ sudo mount -o loop ~/Desktop/ubuntu-7.10-desktop-i386.iso /tmp/livecd
```

然后在工作目录（*〜/ livecd*）中创建一个包含未来CD映像（*cd*）的目录，并将所有CD内容（但*casper / filesystem.squashfs*）复制到我们的〜/ livecd / cd目录中：

```
$ mkdir ~/livecd
$ mkdir ~/livecd/cd
$ rsync --exclude=/casper/filesystem.squashfs -a /tmp/livecd/ ~/livecd/cd
```

这将复制squashfs文件以外的所有文件，该文件是包含我们实时CD文件系统的压缩文件。

现在，我们需要将casper / filesystem.squashfs挂载到名为〜/ livecd / squashfs的目录中，以便将其内容复制到要编辑实时CD文件系统的目录中：*〜/ livecd / custom*

```
$ mkdir ~/livecd/squashfs
$ mkdir ~/livecd/custom
$ sudo modprobe squashfs
$ sudo mount -t squashfs -o loop /tmp/livecd/casper/filesystem.squashfs ~/livecd/squashfs/
$ sudo cp -a ~/livecd/squashfs/* ~/livecd/custom
```

最后，让/etc/resolv.conf和/ etc / hosts复制到我们的〜/ livecd / custom / etc中，这样我们就可以从要定制的映像中访问网络（通过chroot）：

```
$ sudo cp /etc/resolv.conf /etc/hosts ~/livecd/custom/etc/
```

## 进入我们的未来形象：

为了自定义映像，我们将*chroot*更改为〜/ livecd / custom目录，并安装一些必要的伪文件系统（/ proc和/ sys）。从那里，我们将能够自定义Live CD。

```
$ sudo chroot ~/livecd/custom
# mount -t proc none /proc/
# mount -t sysfs none /sys/
# export HOME=/root
```

## 定制我们未来的现场CD

### 移除包装

首先，我们将删除非英语语言包，并且为了释放更多空间，我们将删除gnome-games包。

```
# apt-get remove --purge gnome-games*
 # apt-get remove --purge `dpkg-query -W --showformat='${Package}\n' | grep language-pack | egrep -v '\-en'`
```

请注意，您可能想删除其他一些软件。为了查看已安装的软件，您可以运行以下命令：

```
# dpkg-query -W --showformat='${Package}\n' | less
```

### 更新现有图像

现在我们已经删除了不需要的软件，我们可以更新*/etc/apt/sources.list*以便启用*gutsy-updates*，*gutsy-security*和*合作伙伴*存储库以及*Universe*和*Multiverse*存储库，以便我们可以安装vmware-服务器。

打开并编辑*/etc/apt/sources.list*

```
# vim /etc/apt/sources.list
```

并使它看起来像：

```
deb http://archive.ubuntu.com/ubuntu gutsy main restricted universe multiverse
deb-src http://archive.ubuntu.com/ubuntu gutsy main restricted universe multiverse

deb http://archive.ubuntu.com/ubuntu gutsy-updates main restricted universe multiverse
deb-src http://archive.ubuntu.com/ubuntu gutsy-updates main restricted universe multiverse

deb http://security.ubuntu.com/ubuntu gutsy-security main restricted universe multiverse
deb-src http://security.ubuntu.com/ubuntu gutsy-security main restricted universe multiverse

deb http://archive.canonical.com/ubuntu gutsy partner
deb-src http://archive.canonical.com/ubuntu gutsy partner
```

现在，我们可以通过运行以下命令更新映像：

```
# apt-get update
# apt-get dist-upgrade
```

### 安装新软件包

让我们安装可能需要的所有多媒体包。按照[在Ubuntu下播放DVD](https://www.debuntu.org/how-to-play-dvd-under-ubuntu/)，我们需要安装：

```
# apt-get install gstreamer0.10-ffmpeg gstreamer0.10-plugins-ugly gstreamer0.10-plugins-ugly-multiverse gstreamer0.10-plugins-bad gstreamer0.10-plugins-bad-multiverse vlc mplayer mplayer-fonts
# /usr/share/doc/libdvdread3/install-css.sh
```

然后，安装**RealPlayer**：

```
# wget http://www.debian-multimedia.org/pool/main/r/realplay/realplayer_10.0.9-0.1_i386.deb -O /tmp/realplay.deb
# dpkg -i /tmp/realplay.deb
```

现在，让我们安装一些未在Ubuntu live cd上默认包含的utils，但在大多数情况下它们都非常方便：

```
# apt-get install rar unrar unace-nonfree
```

另外，让我们安装一些无线网络实用程序，以便我们可以通过实时CD来实现无线功能：

```
# apt-get install ndiswrapper-common ndiswrapper-utils-1.9 cabextract unshield \
 bcm43xx-fwcutter \
 kismet aircrack-ng
```

让我们添加其他一些网络网络实用工具：

```
# apt-get install wireshark nmap ettercap traceroute
```

另外，我们将添加一些Firefox插件：

```
# apt-get install flashplugin-nonfree mozilla-plugin-vlc
```

在此之上，我希望能够使用skype和vmware-server：

```
# apt-get install libqt4-core libqt4-gui
# wget http://skype.com/go/getskype-linux-ubuntu -O /tmp/skype.deb
# dpkg -i /tmp/skype.deb
# apt-get install vmware-server
```

好了，就是这样，我们现在拥有使用实时CD时需要的任何软件。

现在是时候进行一些清理了。

## 清理chroot

当我们安装软件包时，*apt*缓存软件包，我们将需要删除它们以节省一些空间：

```
# apt-get clean
```

另外，/ tmp中有一些文件需要删除：

```
# rm -rf /tmp/*
```

在chroot之前，我们添加了2个文件：/ etc / hosts和/etc/resolv.conf，让我们删除它们：

```
# rm -f /etc/hosts /etc/resolv.conf
```

最后，我们准备退出chroot并重新打包CD。我们首先需要卸载/ proc和/ sys：

```
# umount /proc/
# umount /sys/
# exit
```

最后，回到主机，修改了一些软件包，我们需要重建一些清单文件，重新创建squashfs和重新创建ISO。

## 重新创建ISO

Fisrt，让我们重新创建清单文件：

```
$ chmod +w ~/livecd/cd/casper/filesystem.manifest
$ sudo chroot ~/livecd/custom dpkg-query -W --showformat='${Package} ${Version}\n' > ~/livecd/cd/casper/filesystem.manifest
sudo cp ~/livecd/cd/casper/filesystem.manifest ~/livecd/cd/casper/filesystem.manifest-desktop
```

并重新生成squashfs文件：

```
$ sudo mksquashfs ~/livecd/custom ~/livecd/cd/casper/filesystem.squashfs
Parallel mksquashfs: Using 2 processors
Creating little endian 3.0 filesystem on ~/livecd/cd/casper/filesystem.squashfs, block size 65536.
....
....
```

现在，或者，您可能想要自定义文件：〜/ livecd / cd / README.diskdefines

最后，更新〜/ livecd / cd / md5sum.txt，其中包含〜/ livecd / cd md5和中的文件：

```
$ sudo rm ~/livecd/cd/md5sum.txt
 $ sudo -s
 # (cd ~/livecd/cd && find . -type f -print0 | xargs -0 md5sum > md5sum.txt)
```

现在我们差不多完成了，剩下的最后一件事就是也可以使用以下命令创建ISO：

```
$ cd ~/livecd/cd
$ sudo mkisofs -r -V "Ubuntu-Live-Custom" -b isolinux/isolinux.bin -c isolinux/boot.cat -cache-inodes -J -l -no-emul-boot -boot-load-size 4 -boot-info-table -o ~/Desktop/Ubuntu-Live-7.10-custom.iso .
```

在这里，您现在可以通过使用或使用虚拟化/仿真软件（例如qemu，kvm，vmware…）启动计算机来测试映像。

## 结论

只需做一些工作，就可以自定义Ubuntu Live CD，以删除/包含某些软件，确保该Live CD是最新的，并且最重要的是，允许管理员部署预先定制的Ubuntu发行版。 

