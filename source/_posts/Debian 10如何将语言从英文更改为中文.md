---
title: Debian 10 如何将语言从英文更改为中文
comments: true
tags: debian
categories: linux
keywords: 'debian,linux'
abbrlink: d639a0f
description:
---



由于安装的是英文版 Debian 。我英语水平不怎么好，因此需要将默认语言改为中文。这篇文章向大家介绍如何将 Debian 10 Buster 的语言从英文更改为中文。

<!--more-->



## Debian 10如何将语言从英文更改为中文

1. 打开终端并登录 **root** 用户。
2. 使用 **nano** 文本编辑器打开 `/etc/locale.gen` 文件。
3. 如果你在安装的 Debian10 系统时将语言环境设为美国英语，在这个文件中：**en_US.UTF-8 UTF-8** 行首没有 `#` 符号。
4. 要添加中文语言支持，找到 **zh_CN** 开头的那几行。在行首把这几行的 `#` 符号去掉。
5. 保存文件后，在终端执行 `locale-gen` 命令。
6. 打开Gnome设置面板，选择**Region & Language**，双击**Language**，选择**汉语**。
7. 重新登录 Debian10 系统。你将会看见**将标准文件夹更新到当前语言吗？**对话框。它是问你要不要将home目录下文件夹的语言也更改成中文。我的建议是这些文件夹要使用英文。所以点击**保留旧的名称**。

现在Debian的界面语言是中文了，但是有些程序如iceweasal, icedove, libreoffice等需要安装专门的中文语言包。输入下面的命令为这些程序安装中文语言包。

```
apt-get install -y \
	chromium-l10n icedove-l10n-zh-cn \
	iceowl-l10n-zh-cn iceweasel-l10n-zh-cn \
	libreoffice-help-zh-cn libreoffice-l10n-zh-cn \
	debian-faq-zh-cn kicad-doc-zh-cn \
	openvanilla-imgeneric-data-zh-cn \
	texlive-lang-chinese
```

完成以上操作，Debian10汉化工作完成。

