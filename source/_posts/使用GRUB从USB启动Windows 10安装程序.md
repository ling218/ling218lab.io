---
title: 使用 GRUB 从USB启动 Windows 10 安装程序
comments: true
tags: grub
categories: other
keywords: 'windows,grub'
description: 您可以使用这些说明创建可以运行Windows 10安装程序的GRUB的可启动USB驱动器
abbrlink: 2c4a335
---

您可以使用这些说明创建可以运行Windows 10安装程序的GRUB的可启动USB驱动器。我使用Arch Linux来准备我的USB设备，但是任何像Debian或Ubuntu这样的Linux版本都可以运行。

我假设你有一个适当大的USB磁盘`/dev/sdz`，你可以完全擦除这个过程。

如果已安装，请卸载USB驱动器。
```
sudo umount /dev/sdz*
```
擦除USB设备上的所有分区。
```
sudo dd if=/dev/zero of=/dev/sdz bs=512 count=1 conv=notrunc
```
使用`sfdisk`创建两个分区。第一个分区是`500MiB`bootable（`*`）Linux分区，第二个分区是一个占用剩余空间的`ntfs`分区（`7`）。
```
sudo sfdisk /dev/sdz << EOF
,500M,,*
,,7,;
EOF
```
格式化第一个分区，`/dev/sdz1`如`ext4`。
```
sudo mkfs.ext4 /dev/sdz1
```
格式化第二个分区，`/dev/sdz2`如`ntfs`。
```
sudo mkfs.ntfs /dev/sdz2
```
安装在`/dev/sdz1`当地的某处。就我而言，我正在使用`/mnt/part1`。
```
sudo mount /dev/sdz1 /mnt/part1
```
安装在`/dev/sdz2`当地的某处。就我而言，我正在使用`/mnt/part2`。
```
sudo mount /dev/sdz2 /mnt/part2
```
装载Windows 10安装ISO。在我的情况下，我正在安装它`/mnt/win10`。
```
mount -t udf ~/win10.iso /mnt/win10
```
将grub安装到`ext4`分区（`sdz1`）。此命令提供了受Arch Linux文章启发的最小GRUB安装。

我承认我并不完全知道每个模块在这里做了什么，但我选择了一个似乎需要我配置的最小列表。
```
sudo grub-install \
    --no-floppy \
    --target=i386-pc \
    --recheck \
    --debug \
    --locales="en@quot" \
    --themes="" \
    --root-directory=/mnt/part1 \
    --boot-directory=/mnt/part1/grub-boot \
    --install-modules="ntldr normal search ntfs" \
    /dev/sdz
```
`grub.cfg`在`/mnt/part1/grub-boot/grub/grub.cfg`。创建GRUB配置文件。`grub.cfg`基于一些 文章，这是一个相对较小的。
```
# USB Device:/grub-boot/grub/grub.cfg

set timeout=10 set default=0

menuentry "Windows 10 Installer" {
    insmod ntfs
    search --set=root --file /bootmgr
    ntldr /bootmgr
    boot
}
```
将Windows 10安装文件复制到`ntfs`分区（`sdz2`）。
```
rsync -vr /mnt/win10/ /mnt/part2/
```
将文件同步到设备。根据设备的速度，这可能需要一些时间。
```
sync
```
清理。
```
sudo umount /dev/sdz*
```
启动！你应该全力以赴。

如果从USB设备启动到Windows 10时遇到错误，说明：

> 缺少计算机所需的媒体驱动程序。这可以是DVD，USB或硬盘驱动器。如果您有带驱动程序的CD，DVD或USB闪存驱动器，请立即插入。

> 注意：如果Windows的安装介质位于DVD驱动器或USB驱动器上，则可以在此步骤中安全地将其删除。

那么您可能希望查看解决该问题的Microsoft社区文章。您可能还想验证所有文件是否正确复制`rsync`或重新复制文件以防万一有损坏。您可能还需要验证源`Windows 10 iso`文件的完整性。最后，我发现USB 2.0驱动程序对我来说比USB 3.0更可靠，并且当我在我的一台机器上遇到它时解决了这个错误。