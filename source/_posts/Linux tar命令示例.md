---
title: Linux tar 命令示例
comments: true
tags: tar
categories: linux
keywords: 'linux,tar'
description: Debian Linux tar命令示例
abbrlink: 28a0add8
---


## tar命令参数：

- -c 创建压缩文件
- -x 解压文件
- -v 显示进度
- -f 文件名
- -t 查看压缩文件内容
- -j 通过bzip2归档
- -z 通过gzip归档
- -r 在压缩文件中追加文件或目录
- -W 验证压缩文件

## 示例：

```
tar -cvf code.tar /home/abc/code/
tar cvzf code.tar.gz /home/abc/code/
tar cvfj code.tar.bz2 /home/abc/code/
tar xvf code.tar -C /home/abc/code
tar xvf code.tar.gz
tar xvf code.tar.bz2
tar tvf code.tar
tar --extract --file=code.tar Readme.txt  解压tar包中的单个文件
tar -xvf code.tar "file1" "file2"
tar -rvf code.tar abc.txt 在tar包中加入文件
tar -rvf code.tar /home/abc/doc 在tar包中加入目录 
```

