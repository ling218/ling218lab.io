---
title: 适用于Linux的Windows子系统的中文化
comments: true
tags: shell
categories: linux
keywords: shell
abbrlink: d02e6c08
description: 实操记录
---


中文化和用户环境定制

## 中文化操作
```
# 更新Linux系统
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
# 安装软件包
sudo apt-get -y install task-chinese-t openssh-server openssh-client curl git 
# 中文环境设置
sudo dpkg-reconfigure locales 
sudo update-locale --reset
sudo update-locale LANG=zh_CN.UTF-8 
# 为Windows共享文件夹创建挂载点
sudo mkdir /mnt/share
```

## 用户环境设置
```
# 用户环境设置
echo -e "set number\nset tabstop=4\nset list\nset listchars=tab:>_" > ~/.vimrc
echo -e "location\nprogress-bar\nremote-time\nshow-error" > ~/.curlrc
# 启动SSH
sudo /etc/init.d/ssh start
# 挂载Windows共享文件夹
sudo mount -t drvfs '\\server\share' /mnt/share
# 命令自动补全
sudo apt-get install bash-completion
cat <<EOF > /etc/profile
if [ -f /etc/bash_completion ]; then
. /etc/bash_completion
fi
EOF
## 使其生效
source /etc/profile
```


## 我的项目地址
- <https://gitlab.com/ling218>