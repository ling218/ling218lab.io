---
title: 清除 Debian/Ubuntu 系统 DNS 缓存
comments: true
tags: DNS
categories: linux
keywords: 'linux,DNS'
description: >-
  当你访问一个网站时，需要对网站域名进行解析，也就是获得域名指向的IP。Linux系统有DNS
  Cache机制，缓存了域名对应的IP，这样再下次访问同一个网站时，就不再需要执行域名查询，提高访问速度。
abbrlink: e9ef5079
---



当你访问一个网站时，需要对网站域名进行解析，也就是获得域名指向的IP。Linux系统有DNS Cache机制，缓存了域名对应的IP，这样再下次访问同一个网站时，就不再需要执行域名查询，提高访问速度。

有些网络问题可以通过清理系统DNS缓存来解决，如本地缓存的域名对应错误的IP。

#### Debian/Ubuntu清理DNS缓存
```
sudo /etc/init.d/dns-clean
```

dns-clean其实就是bash脚本，如下:
```
PATH=/sbin:/bin:/usr/sbin:/usr/bin
 
test -f /usr/sbin/pppconfig || exit 0
mkdir /var/run/pppconfig >/dev/null 2>&1 || true
test -f /etc/ppp/ip-down.d/0dns-down || exit 0
 
case "$1" in
  start)
        /bin/echo -n "Running 0dns-down to make sure resolv.conf is ok..."
        /etc/ppp/ip-down.d/0dns-down "0dns-clean" && /bin/echo "done."
        ;;
  stop|restart|force-reload)
        ;;
  *)
        ;;
esac
 
exit 0
```