---
title: Ubuntu 18.04 安装中文版后的配置
comments: true
tags: ubuntu
categories: linux
keywords: 'ubuntu,linux'
description: 这是我安装 Ubuntu 18.04 后的配置
abbrlink: 647419be
---

## 更换成中国apt服务器
```
less /etc/apt/sources.list
```
## apt软件包更新
```
apt update         #更新存储库列表
apt upgrade        #更新软件包
apt full-upgrade   #更新软件包
apt autoremove     #不再需要的软件包删除
```

## 从中文目录名称更改为英文
```
env LANGUAGE=C LC_MESSAGES=C xdg-user-dirs-gtk-update
```

## 卸载亚马逊
```
apt remove --purge ubuntu-web-launchers
```

## 卸载libreoffice
```
apt remove --purge libreoffice-common
```

## 在Ubuntu 18.04 上禁用系统错误报告消息
```
nano /etc/default/apport

enabled=0
```

## google chrome
**下载google chrome:**https://www.google.co.jp/chrome/browser/desktop/index.html
```
apt install ./google-chrome-stable_current_amd64.deb
```

## VSCode
**下载VSCode：** https://code.visualstudio.com/
```
apt install ./code_*_amd64.deb
```

## Vim
```
apt install vim
```

