---
title: Debian 使用 curl 获得外部 IP 地址（公网地址）
comments: true
tags: curl
categories: linux
keywords: 'linux,curl'
description: Debian 使用 curl 获得外部 IP 地址（公网地址）
abbrlink: 7cfdd891
---


执行下面命令获得：

```
# curl icanhazip.com   #速度快点
# curl ipinfo.io/ip   #速度稍慢
```

