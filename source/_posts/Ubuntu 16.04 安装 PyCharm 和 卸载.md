---
title: Ubuntu 16.04 安装 PyCharm 和卸载
comments: true
tags: 'pycharm,ubuntu'
categories: linux
keywords: 'linux,pycharm'
description: Ubuntu 16.04 安装 PyCharm 和 卸载
abbrlink: c5acc6df
---

### Ubuntu 16.04 安装 PyCharm 和 卸载


1、添加 PyCharm 软件源。

```
# sudo add-apt-repository ppa:mystic-mirage/pycharm
```

2、安装 PyCharm 专业版。

```
# sudo apt-get update
# sudo apt-get install pycharm
```

3、安装 PyCharm 社区版。

```
# sudo apt-get update
# sudo apt-get pycharm-community
```

4、卸载 PyCharm 。

```
# apt remove pycharm
# apt remove pycharm-community
# apt autoremove
```

