---
title: Kali Linux：从 light 升级到完整版
comments: true
tags: kali
categories: linux
keywords: 'kali,light'
abbrlink: cee69b17
description:
---



找了半天，我只找到了一个2G的U盘。

由于空间不足，安装不了Kali Linux的完整版（2.8G），只能安装Light（轻量）版（0.8G）。

Kali Linux Light版使用了轻量级的xfce桌面环境，自带的工具也不全。

那么，有没有办法升级到完整版呢？

<!--more-->



## 步骤

安装`kali-linux-full`，包含各种工具：
```
sudo apt-get install kali-linux-full
```
安装 GNOME3 桌面环境：
```
sudo apt-get install kali-desktop-gnome
```
如果你想要卸载 Kali light 使用的 XFCE 桌面环境：
```
sudo apt-get purge kali-desktop-xfce
```