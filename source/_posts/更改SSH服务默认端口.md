---
title: 更改SSH服务默认端口
comments: true
tags: ssh
categories: linux
keywords: 'linux,ssh'
description: SSH服务默认使用22端口，为了防止端口扫描及暴力破解，我们可以更改SSH的默认端口
abbrlink: ebc5ab1c
---



SSH服务默认使用22端口，为了防止端口扫描及暴力破解，我们可以更改SSH的默认端口。

## 更改SSH服务默认端口(Linux)

编辑ssh配置文件：

```
sudo vim /etc/ssh/sshd_config
```

找到如下一行：

```
Port 22
```

更改你要使用的端口，如：

```
Port 3399
```

如果你操作的是远程主机要格外小心，确保没有其它程序使用这个端口并检查防火墙。一旦出错，可能导致SSH连接不上远程服务器。

更改完成之后，重启SSH服务：

```
sudo systemctl restart ssh
```

查看SSH服务监听的端口：

```
sudo netstat -tunlp | grep ssh
```

连接时需使用-p选项指定端口：

```
ssh root@server_domain_OR_IP -p 3399
```