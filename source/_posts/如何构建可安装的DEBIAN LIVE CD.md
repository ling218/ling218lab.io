---
title: 如何构建可安装的 DEBIAN LIVE CD
comments: true
tags: live
categories: live-build
keywords: live-build
abbrlink: fa8c066e
description: 整理记录
---

## 它是什么？
Live-build（以前称为live-helper）是一个命令行工具，其中包含用于从配置目录构建实时系统的组件。它是一组脚本，用于完全自动化地构建debian live系统映像，并自定义构建Live映像的各个方面。安装时，Live-build会使用三个命令：lb clean清理chroot环境，lb config指定构建选项以及lb build开始构建。显然lb config，这将是唯一困难的命令。

让我们从您最喜欢的终端仿真器开始，并在您想要的任何目录下工作（我从mate-terminal转储我的位置开始/home/user）。

## 安装实时构建工具
```
su -
apt-get update
apt-get install live-build 
```

## 使用实时构建
现在，我们为现场CD项目创建一个文件夹，以使所有内容都保持良好状态。
```
mkdir live
cd live
lb clean
```

现在，我们为lb config创建脚本。
使用除config之外的任何名称保存此脚本，否则lb config将无法创建/usr/lib/live/build/config/common（我称为tempy）。
```
#!/bin/bash
lb config \
--mode debian \
--system live \
--interactive shell \
--distribution jessie \
--debian-installer live \
--architecture i386 \
--archive-areas "main contrib non-free" \
--security true \
--updates true \
--binary-images iso-hybrid \
--memtest memtest86+ \
```
特殊考虑：
该脚本主要是自我解释。当然，它指定的是debian，jessie，debian的实时安装程序，32位（如果您想要64位构建，请更改--architecture i386为--architecture amd64，存储库的所有区域，所有更新，isohybrid（在具有持久性的闪存驱动器上工作）和memtest86 +。奇怪的是--interactive shell；作为构建过程的一部分，您将被转储到chroot环境中以配置新的发行版。您将需要这样做，以便可以在将系统压缩到最小容量之前安装软件包并进行其他配置。 iso。

通过lb config对手册中其他选项的大量故障排除，我发现诸如--checksums和的其他选项--grub-splash不起作用。它们是需要固定的选项（我想还有很多其他选项），但是确实可以轻松解决，例如…

构建完iso之后，用于md5sum生成md5校验和。对于--grub-splash，请转到/usr/share/live/build/bootloaders，您将看到一堆用于引导加载程序配置的文件夹。进入每个文件夹并删除splash.svg，并用所需的任何图像替换，只要它被调用即可splash.png。

回到它。
config需要成为可执行文件。
`chmod 700 tempy`
然后我们执行。
`./tempy`
让我们开始吧。
lb build
不要忘记我们--interactive shell在执行的脚本中指定了。输入该命令lb build将开始构建过程，下载所需的一切并将它们放在一起，最终将您转储到chroot环境中进行更多配置。
我们现在要安装软件包。我将为此使用分发包列表。
```
apt-get install task-ssh-server task-laptop task-print-server xorg alsa-base laptop-mode-tools cryptsetup live-config-systemd tor firmware-linux-nonfree firmware-ralink firmware-realtek firmware-atheros firmware-iwlwifi firmware-brcm80211 firmware-b43-installer flashplugin-nonfree lightdm mate-desktop-environment-core caja ufw dconf-editor caja-gksu caja-open-terminal mate-user-share mate-applets mate-settings-daemon mate-utils mate-control-center mate-notification-daemon mate-system-tools mate-screensaver mate-power-manager mate-system-monitor openvpn network-manager-gnome samba hardinfo pavucontrol leafpad iceweasel libreoffice-calc libreoffice-writer vlc gimp atril pidgin deluge gparted gdebi brasero engrampa galculator gpicview gksu synaptic xsane system-config-printer grub2-common frozen-bubble chromium-bsu quadrapassel gweled remmina xrdp extlinux florence
```
特别注意事项：请
密切注意，您将看到包装grub2-common。这是为了节省时间，无需找出偶数--bootloader选项是否lb config有效。如果我们现在不在软件包中包含引导加载程序，您将看到生成的构建中的debian安装程序无法安装引导加载程序。这就是为什么我们现在在此包括此软件包的原因。

是的，我们要安装这些软件包。
其他配置。
一旦安装了所需的所有软件包，我们就可以在这里完成构建。但是，您可能还不想。我推荐三件事。首先在内部/etc/Network-manager/Network-manager.conf并将其更改为managed=false，managed=true以便网络管理员可以管理网络接口。

第二，我还建议配置构建的默认用户参数。为此，您可以将用户主目录的全部内容复制到/live/chroot/etc/skel（隐藏的文件夹和所有文件夹）中。我要做的是在系统上创建一个新用户，然后进行配置。屏幕保护程序，电源设置，程序规范，主题等。然后，我收获新用户主目录的内容（隐藏的文件夹和所有内容；请不要提供个人文档），然后将它们全部放入/live/chroot/etc/skel。现在，您在构建中已为每个将来的用户指定了查看桌面的方式。

第三是安装在debian仓库中找不到的软件包。再次，如前所述；我们可以将文档复制到chroot环境。一旦安装了这些额外的软件包，请删除软件包文件，因为它们将显示在最终产品的文件结构中，这对将来的用户或您自己忘记的情况都没有意义。

如果需要，您可以继续配置其他内容。例如更名（我不在这里，因此我不在此关注）。但是，如果您对事情的现状感到满意，请继续执行下一步。

完成构建。 这样，您将退出chroot环境，并通过下载所需的任何内容完成其工作，并最终将构建压缩到文件系统中，然后将其包含在iso文件中。
```
exit
lb build
```
命名您的iso，创建md5校验和文件，然后仔细检查md5校验和。 我们已经重命名了iso，创建了一个md5校验和文件，并将md5校验和与iso（通过了它）进行了比较。
```
mv live-image-i386.hybrid.iso mybuild.iso
md5sum mybuild.iso > mybuild.md5
md5sum -c mybuild.md5
```

**原文地址：**
- https://terkeyberger.wordpress.com/2016/05/14/live-build-how-to-build-an-installable-debian-live-cd/




