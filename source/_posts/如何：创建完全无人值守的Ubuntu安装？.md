---
title: 如何：创建完全无人值守的 Ubuntu 安装
comments: true
tags: ubuntu
categories: live-build
keywords: 'ubuntu,live-build'
abbrlink: 9e84e35
description:
---



把Ubuntu镜像文件刻录到CD或U盘存储介质中，插入计算机上，引导起动，无需手动操作，自动安装Ubuntu。

<!--more-->



## 完整的解决方案是：

重新制作CD，即下载非图形Ubuntu安装ISO镜像文件，并挂载。

```
# sudo su -
# mkdir -p /mnt/iso
# mount -o loop ubuntu.iso /mnt/iso
```

将相关文件复制到其他目录。

```
# mkdir -p /opt/ubuntuiso
# cp -rT /mnt/iso /opt/ubuntuiso
```

防止出现语言选择菜单。

```
# cd /opt/ubuntuiso
# echo en > isolinux/lang
```

使用GUI程序添加名为的kickstart文件 `ks.cfg` 。

```
# apt-get install system-config-kickstart
# system-config-kickstart # save file to ks.cfg
```

要为安装添加软件包，请在kickstart文件中添加一个`%package`部分，然后在`ks.cfg`文件末尾附加`ks.cfg`类似内容。

```
%packages
@ ubuntu-server
openssh-server
ftp
build-essential
```

这将安装Ubuntu的服务器“捆绑”，并会增加的`openssh-server`，`ftp`和`build-essential`包。

添加预置文件，以消除其他问题。

```
# echo 'd-i partman/confirm_write_new_label boolean true
d-i partman/choose_partition \
select Finish partitioning and write changes to disk
d-i partman/confirm boolean true' > ks.preseed
```

设置启动命令行以使用kickstart和预置文件。

```
# vi isolinux/txt.cfg
```

搜索：

```
label install
  menu label ^Install Ubuntu Server
  kernel /install/vmlinuz
  append  file=/cdrom/preseed/ubuntu-server.seed vga=788 initrd=/install/initrd.gz quiet --
```

添加`ks=cdrom:/ks.cfg`并添加`preseed/file=/cdrom/ks.preseed`到附加行。您可以删除`quiet`和`vga=788`词。它看起来像。

```
  append file=/cdrom/preseed/ubuntu-server.seed \
     initrd=/install/initrd.gz \
     ks=cdrom:/ks.cfg preseed/file=/cdrom/ks.preseed --
```

现在创建一个新的ISO。

```
# mkisofs -D -r -V "ATTENDLESS_UBUNTU" \
     -cache-inodes -J -l -b isolinux/isolinux.bin \
     -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 \
     -boot-info-table -o /opt/autoinstall.iso /opt/ubuntuiso
```

现在，您将拥有一张CD。该CD可在从Ubuntu系统引导时安装，而无需一次击键。