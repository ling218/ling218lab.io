---
title: ssh 允许使用root用户登录Linux
comments: true
tags: ssh
categories: linux
keywords: 'linux,ssh'
description: 配置允许使用 root 远程登陆linux主机
abbrlink: a242a680
---



本文介绍如果在正确安装 ssh 服务的前提下，配置允许使用 root 远程登陆 Debian 。

1、编辑`/etc/ssh/sshd_config` ssh服务配置文件。

```
PermitRootLogin yes  #允许root远程登陆
PasswordAuthentication yes  #开启秘钥和密码认证
```

2、保存配置后，重启ssh服务。

```
# /etc/inin.d/ssh restart  #重启ssh服务
```

3、以 root 用户登陆ssh服务示例。

```
# ssh root@x.x.x.x
```

