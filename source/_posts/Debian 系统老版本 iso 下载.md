---
title: Debian 系统老版本 iso 下载
comments: true
tags: iso
categories: linux
keywords: 'linux,iso'
description: Debian 系统老版本 iso 下载
abbrlink: b2adb537
---

## Debian 系统老版本 iso 下载

debian官方默认只提供最新的版本下载，有时需要找一些旧的版本的debian，着实费用。下面这个debian iso镜像站可以下载各个版本的debian下载：[debian 系统各个版本ISO下载](http://cdimage.debian.org/cdimage/archive/)