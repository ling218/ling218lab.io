---
title: 如何更改linux文件的拥有者及用户组(chown和chgrp)
comments: true
tags: chown
categories: linux
keywords: 'chown,linux'
abbrlink: 1b5f6a82
description: 整理记录
---

在Linux中，创建一个文件时，该文件的拥有者都是创建该文件的用户。该文件用户可以修改该文件的拥有者及用户组，当然root用户可以修改任何文件的拥有者及用户组。在Linux中，对于文件的权限（rwx），分为三部分，一部分是该文件的拥有者所拥有的权限，一部分是该文件所在用户组的用户所拥有的权限，另一部分是其他用户所拥有的权限。

在shell中，要修改文件当前的用户必须具有管理员root的权限。可以通过su命令切换到root用户，也可以通过sudo获得root的权限。



## chown 命令更改文件拥有者
在 shell 中，可以使用chown命令来改变文件所有者。chown命令是change owner（改变拥有者）的缩写。需要要注意的是，用户必须是已经存在系统中的，也就是只能改变为在 `/etc/passwd` 这个文件中有记录的用户名称才可以。

chown命令的用途很多，还可以顺便直接修改用户组的名称。此外，如果要连目录下的所有子目录或文件同时更改文件拥有者的话，直接加上`-R`的参数即可。

**语法：**
- `chown -R 账号名称 文件或目录`
- `chown -R 账号名称:用户组名称 文件或目录`
- `-R` : 进行递归( recursive )的持续更改，即连同子目录下的所有文件、目录都更新成为这个用户和用户组。常常用在更改某一目录的情况。

> `ls -l` 查看文件或目录所属用户和组

## chgrp 命令更改文件所属用户组
在shell中，可以使用chgrp命令来改变文件所属用户组，该命令就是change group（改变用户组）的缩写。需要注意的是要改变成为的用户组名称，必须在 /etc/group里存在，否则就会显示错误。
**语法：**
- `chgrp -R 用户组名称 dirname/filename `
- `-R` : 进行递归( recursive )的持续更改，即连同子目录下的所有文件、目录都更新成为这个用户和用户组。常常用在更改某一目录的情况。