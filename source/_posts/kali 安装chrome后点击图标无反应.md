---
title: kali 安装chrome后点击图标无反应
comments: true
tags: google-chrome
categories: linux
keywords: chrome
description: google-chrome安装问题
abbrlink: 1e2bfe60
---



是因为 Chrome 不能以 root 运行的原因，解决方式如下：

用hexedit去编辑chrome文件，把`geteuid`改`getppid`。

1. 搜索并安装hexedit:

    ```
    # apt-get hexedit
    ```

2. 用hexedit修改`/opt/google/chrome/chrome`文件,如果提示文件为只读，采用以下方法：

    ```
    # cp chrome /root/
    # hexedit /root/chrome
    ```

    这样便可以修改/root/目录下的chrome了，打开chrome，按[Tab键](https://www.baidu.com/s?wd=Tab键&tn=SE_PcZhidaonwhc_ngpagmjz&rsv_dl=gh_pc_zhidao)，再按`Ctrl+s`搜索`geteuid`，找到后，直接输入`getppid`，将会覆盖掉`geteuid`，最后按`Ctrl+x`保存。

    > 注意：路径如果不同的话请自行使用`dpkg -c` 安装包来察看安装到哪里了。

3. 将`/root`目录下的chrome复制回`/opt/google/chrome/`，覆盖掉原来的chrome文件即可。打开chome，现在可以正常使用了。