---
title: 如何在 Debian 9 上安装 Google Chrome Web 浏览器
comments: true
tags: debian
categories: linux
keywords: 'debian,google'
description: 让 Debian 安装 Google 浏览器
abbrlink: 1dfe69d7
---

Google Chrome 浏览器是世界上最爱欢迎的网络浏览器。快速、直观和安全的浏览器。

Chrome 不是一个开源浏览器，它不包含在 Debian 存储库中。

Google Chrome 浏览器基于 Chromium，这是一个开源浏览器，可在 Debian 存储库中找到。

在本文章中，我将介绍如何在 Debian 9 上安装 Google Chrome 浏览器。


## 要求：

- 安装 Debian 9 系统的主机
- Google Chrome 浏览器
- 具有 sudo 访问权限的用户
- 时间和耐心



## 安装：

完成以下步骤实现在 Debian 系统上安装 Google Chrome 浏览器：

### 下载 Google Chrome 浏览器

使用 `Ctrl+Alt+T` 键盘快捷键或单击终端图标打开终端。

运行以下 `wget` 命令下载最新 Google Chrome 浏览器 `.deb` 包。

```console-bash
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
```

### 安装 Google Chrome 浏览器

下载完成后，键入以下内容安装 Google Chrome 浏览器。

```console-bash
sudo apt install ./google-chrome-stable_current_amd64.deb
```

系统将要求您输入用户密码，上面的命令将安装 Chrome 和 所有依赖项。

### 启动 Google Chrome 浏览器

现在您已经在 Debian 系统上安装了 Google Chrome，你可以通过输入 `google-chrome` 或点击 Google Chrome 图标 启动它。

### 更新 Google Chrome 浏览器

在安装过程中，官方 google 存储库将添加到系统中，可以使用 `cat` 命令验证文件内容：

```
cat /etc/apt/sources.list.d/google-chrome.list
```

```
deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main
```

发布新版本后，可以通过在终端中运行以下命令来更新 Google Chrome 软件。

```
sudo apt-get update
sudo apt-get upgrade
```

