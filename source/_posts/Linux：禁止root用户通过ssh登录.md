---
title: Linux：禁止 root 用户通过 ssh 登录
comments: true
tags: ssh
categories: linux
keywords: ssh
abbrlink: 1e7ce50
description:
---



禁止root用户通过SSH登录，可以提高Linux系统的安全。你也许注意到Linux服务器的登录认证日志记录了很多尝试登录，这是有人使用暴力破解软件留下的。如果关闭了root用户登录，你的服务器就免疫了这种攻击方式。

<!--more-->



SSH服务配置文件有两个， ssh_conf 和 sshd_conf。ssh_conf配置向外的ssh连接（例如，你已经连接到服务器，然后再通过SSH连接到其他服务器）。sshd_conf配置连接到服务器的规则。

## 修改sshd_conf文件

在修改前备份，绝对是一个好主意。编辑：

    vim /etc/ssh/sshd_config


找到如下一行：

    PermitRootLogin yes

更改为：

	PermitRootLogin no

如果该行前有注释（#），要去掉。

到此，禁止root用户通过ssh登录，设置完成。

## 创建一个新用户

我们禁用root用户登录，不代表我们不能执行root操作。下面我们专门创建一个新用户，在使用SSH登录之后可以切换到root。当然你以可以使用已有的用户。

### 1.添加用户

	adduser myssh

### 2.为用户设置密码

	passwd myssh

### 3.把myssh用户添加到Wheel组里

	usermod -a -G wheel myshh

或，编辑/etc/group文件。

## 重启SSH服务

	service sshd restart

使用myssh用户登录之后，使用`su -`切换到root用户。