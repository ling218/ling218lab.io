---
title: Git 学习笔记
comments: true
tags: git
categories: other
keywords: 'git,github'
description: Git学习记录
abbrlink: 3c3cdb74
---


## 一、设置账户名和账户邮箱

```
git config --global user.name "name"
git config --global user.email "name@gmail.com
git config --global http.proxy http://example.com:port
```

## 二、验证账户名和账户邮箱

```
git config --global user.name
git config --global user.email
```

## 三、初始化仓库

```
git init  #会生成 .git 文件夹，关于这个仓库的配置
```

### 四、添加文件及文件夹

```
git add name
# 添加所有文件
git add .
```

### 五、添加描述(必须添加)

```
git commit -m "hello world"
```

### 六、查看状态

```
git status
```

### 七、查看修改的文件

```
git diff -- filename
```

### 八、撤销修改

```
git checkout filename
```

### 九、撤销修改(针对GIT ADD提交之后的)

```
git reset HEAD filename
git checkout filename
```

### 十、查看提交记录

```
git log
git log logid -1 -p
# -1  表示只看到上面的一条记录
# -p 表示查看这条记录的所有信息
```

### 十一、上传文件

```
git remote add origin github仓库地址
# 如遇"fatal: remote origin already exists."此问题
# 输入 git remote rm origin
git push origin master
```

### 十二、分支

```
#查看分支
git branch
#当前分支前面会有一个*
#创建分支
git branch version1.0
#切换分支
git checkout version1.0
#将version1.0同步到当前分支
git merge version1.0
#删除分支
git branch -D version1.0
#克隆远程项目
git clone git@github.com:name/项目名.git
#将当前修改上传到服务器
git push origin master
#将远程修改同步到本地
git fetch origin master
#注：此方法不会将修改同步到当前分支，而是会同步到origin/master分支上，需要使用git merge origin/master同步到当前分支
git pull origin master
#此方法是fetch和merge两者的合并
```

### 十三、其他

```
#创建密钥
ssh-keygen -t rsa -C "name@gmail.com"
#将公钥添加到github
cat .ssh/id_rsa.pub
```

