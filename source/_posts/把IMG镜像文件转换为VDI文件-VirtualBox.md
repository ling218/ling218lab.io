---
title: 把 IMG 镜像文件转换为 VDI 文件-VirtualBox
comments: true
tags: virtualbox
categories: other
keywords: 'IMG,VDI'
description: IMG 镜像转 VDI
abbrlink: 3454c8ba
---


假如你下载了一个IMG格式的系统镜像,然后想使用VirtualBox启动,这时你苦逼的发现VirtualBox不支持IMG格式。为了能在VirtualBox上启动这个IMG镜像,我们可以把它转换为VDI格式的镜像,洋文全称 Virtual Disk Image,这是VirtualBox默认使用的硬盘镜像格式。

> IMG镜像文件和ISO文件类似,QEMU虚拟机把IMG做为默认磁盘映像格式。

## 使用VirtualBox把IMG文件转换为VDI文件

首先确保已安装VirtualBox。

需要使用的命令:
```
VBoxManage -v

5.0.28r111378
```
语法:
```
VBoxManage convertdd input.img output.vdi
```
示例:
```
VBoxManage convertdd eos3.0.base.img eos.vdi
```
根据转换文件大小,你可能需要等待几分钟。

新建虚拟机时只需选择’使用已有的虚拟硬盘文件。