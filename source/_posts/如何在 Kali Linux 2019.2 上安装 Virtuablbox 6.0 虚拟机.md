---
title: 如何在 Kali Linux 2019.2 上安装 VirtualBox 6.0 虚拟机
comments: true
tags: kali
categories: linux
keywords: 'kali,virtualbox'
description: 安装 Virtualbox 6.0
abbrlink: 719b2708
---

Virtualbox 是一种开源的跨平台虚拟化软件。能够安装多个客户端操作系统，每个客户端系统皆可独立打开、暂停与停止。

在本文中介绍如何在 Debian 9 上安装最新的 VirtualBox 6.0。


## 要求：

- 安装 Kali Linux 2019.2 系统的主机
- VirtualBox 6.0 虚拟化软件 
- 时间和耐心



## 安装：

在 Kali Linux 上安装 VirtualBox 是一个相对简单的过程，只需几分钟：

### 首先，配置软件源列表

使用 `Ctrl+Alt+T` 键盘快捷键或单击终端图标打开终端。

```
cat << EOF >> /etc/apt/sources.list

deb http://security.debian.org/ stretch/updates main
deb-src http://security.debian.org/ stretch/updates main

deb http://mirrors.163.com/debian stretch main
deb-src http://mirrors.163.com/debian stretch main

deb http://ftp.cn.debian.org/debian stretch main
deb-src http://ftp.cn.debian.org/debian stretch main

EOF
```

### 下载 VirtualBox 虚拟化软件

运行以下 `wget` 命令下载最新 VirtualBox 虚拟化软件 `.deb` 包。

```
wget https://download.virtualbox.org/virtualbox/6.0.8/virtualbox-6.0_6.0.8-130520~Debian~stretch_amd64.deb
```

### 安装 VirtualBox 虚拟化软件

下载完成后，键入以下内容安装 VirtualBox 虚拟化软件。

```
apt-get update
apt install libcurl3
apt-get install ./virtualbox-6.0_6.0.8-130520~Debian~stretch_amd64.deb
```

### 安装 VirtualBox 扩展包

VirtualBox Extension Pack为客户机提供了一些有用的功能，如虚拟USB 2.0和3.0设备，支持RDP，图像加密等。

运行以下 wget 命令下载 扩展包文件。

```console-bash
wget https://download.virtualbox.org/virtualbox/6.0.0/Oracle_VM_VirtualBox_Extension_Pack-6.0.0.vbox-extpack
```

下载完成后，键入以下内容安装扩展包。

```console-bash
VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-6.0.0.vbox-extpack
```

将看到 Oracle 许可证，并提示接受他们的条款和条件，键入 `y` 并 `Enter`  。

### 启动 VirtualBox 虚拟化软件

现在您已经在 Kali Linux 系统上安装了 VirtualBox，你可以通过输入 `virtualbox` 或点击 VirtualBox 图标启动它。

### 更新 VirtualBox 虚拟化软件

使用 `echo` 命令将官方 virtualbox 存储库添加到系统中，可以使用 `cat` 命令验证文件内容：

```
echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian stretch contrib" > /etc/apt/sources.list.d/virtualbox.list
```

验证文件内容：


```
cat /etc/apt/sources.list.d/virtualbox.list
```

```
deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian stretch contrib
```

添加gpg密钥。

```
wget -q https://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | apt-key add -
wget -q https://download.virtualbox.org/virtualbox/debian/oracle_vbox_2016.asc -O- | apt-key add -
```

发布新版本后，可以通过在终端中运行以下命令来更新 VirtualBox 软件。

```
apt-get update
apt-get upgrade
```

