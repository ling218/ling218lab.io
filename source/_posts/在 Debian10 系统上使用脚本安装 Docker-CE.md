---
title: 在 Debian10 系统上使用脚本安装 Docker-CE
comments: true
tags: docker-ce
categories: linux
keywords: 'linux,docker-ce'
description: 自动化脚本安装docker-ce
abbrlink: c0d5ccf9
---



Docker 在 get.docker.com和test.docker.com上提供了安装脚本，用于快速安装Docker-CE。

get.docker.com上的脚本是最新版本Docker-CE。

test.docker.com上的脚本是最新测试版本Docker-CE。 

在本地运行它们之前，请务必检查从internet下载的脚本。

如果不是在新主机上首次安装 Docker-CE，为确保安装不会出错，可以执行卸载旧版本操作。

## 卸载旧版本

Docker 的旧版本被称为`docker`，`docker.io`或`docker-engine`。如果已安装，请卸载它们：

```
sudo apt-get remove docker docker-engine docker.io containerd runc
```

## 安装步骤

```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

如果想要以非root用户身份使用Docker，可以用以下方式将用户添加到 docker 组：

```
sudo usermod -aG docker your-user
```

注销重新登录用户才能生效！

### 知识点：

> 它会自动在`DEB`基于分发的版本上启动。
>
> 在 `RPM`基于发行版的系统上，您需要使用相应的`systemctl`或`service`命令手动启动它 。如消息所示，默认情况下，非root用户不能运行Docker命令。

## 升级Docker-CE

1. 卸载docker-ce软件包：

   ```
   sudo apt-get purge docker-ce
   ```

2. 主机上的映像，容器，卷或自定义配置文件不会自动删除。要删除所有图像，容器和卷：

   ```
   sudo rm -rf /var/lib/docker
   ```

   您必须手动删除所有已编辑的配置文件。

3. 然后再重新安装Docker-CE。

