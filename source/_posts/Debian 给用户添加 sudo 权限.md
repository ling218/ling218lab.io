---
title: Debian 给用户添加 sudo 权限
comments: true
tags: sudo
categories: linux
keywords: 'linux,sudo'
description: Debian 给用户添加 sudo 权限
abbrlink: c779294b
---

## Linux 给用户添加 sudo 权限

1、给 `/etc/sudoers ` 添加写权限。

```
# chmod +w /etc/sudoers   
```

2、编辑 `/etc/sudoers` 文件，给 **username** 用户添加 sudo 权限。

```
username ALL=(ALL) ALL
```

3、去除 `/etc/sudoers` 写权限。

```
# chmod -w /etc/sudoers
```

上面操作完成后，**username** 用户就拥有了sudo权限了。