---
title: Debian10 环境下搭建 Hexo 个人博客
comments: true
tags: hexo
categories: blog
keywords: 'hexo,next7.6,linux'
description: 搭建属于自己的博客网站
abbrlink: 752f7472
---



## 安装 nodejs

**1、安装nodejs**

```
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt-get install nodejs -y
```

**2、检测nodejs是否安装成功**

```
node -version      #显示版本号
npm -version       #显示版本号
```



## 安装 git

**1、更新安装源缓存并安装 git**

```
apt-get update
```

2、检测git是否安装成功

```
git --version        #显示版本号
```



## Hexo 安装及初始化

**1、安装hexo**

```
npm install hexo-cli hexo-server hexo-deployer-git -g
```

**2、初始化hexo项目**

```
hexo init <blog_path_folder>
```

**3、hexo部署及生成**

```
hexo d -g
```



## 设置主题 （这里Next7.6）

**1、下载主题**

```
cd <you_hexo_blog_path>
git clone https://github.com/theme-next/hexo-theme-next themes/next
```

**2、启用主题**

编辑 Hexo 站点配置文件 `_config.yml` ,找到 `theme` 字段，并将其值更改为 next。

```
theme: next
```