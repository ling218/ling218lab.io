---
title: Debian 安装 mate 桌面环境的方法
comments: true
tags: mate
categories: linux
keywords: 'linux,mate'
abbrlink: 5eedfe66
description:
---

mate是一个轻量级的桌面环境，占用系统资源非常少，正常使用下，大约占300MB左右，界面做得非常漂亮，本人十分喜爱，也是我最喜欢的桌面环境。安装了其他桌面(如：gnome) 后，怎么安装mate?



<!--more-->



## 操作步骤：

```
apt-get install task-mate-desktop  
apt-get install mate-desktop-environment-extras  #完全安装mate及扩展部分
```

执行命令后，网速不慢，几分钟就完事，注销选择mate，进入后即是mate了。

