---
title: 项目：创建自定义Kali Linux ISO
comments: true
copyright: true
tags: live-build
categories: live-build
keywords: 'live-build,kali,linux,自定义'
description: 在现有的Kali环境中构建自定义的Kali ISO
abbrlink: 8feaf38e
---


该存储库将允许您构建自定义的Kali ISO。主要好处是可以将其用作[可启动USB驱动器](https://docs.kali.org/downloading/kali-linux-live-usb-install)。

## 环境准备

理想情况下，您应该**在现有的Kali环境中**构建自定义的Kali ISO **。**建议使用[轻量级](https://www.kali.org/downloads/)或虚拟化（[VMware / VirtualBox](https://www.offensive-security.com/kali-linux-vm-vmware-virtualbox-hyperv-image-download/)）映像。

作为`root`：

```
apt-get install curl git live-build cdebootstrap
```

## 自定义

我在[kali-config / variant-custom](https://github.com/prateepb/kali-live-build/blob/master/kali-config/variant-custom)目录下使用了一个自定义变量。这包括：

- [来自默认变体](https://github.com/prateepb/kali-live-build/blob/master/kali-config/variant-custom/package-lists/kali.list.chroot)（`kali-config/variant-custom/package-lists/kali.list.chroot`）的[所有软件包](https://github.com/prateepb/kali-live-build/blob/master/kali-config/variant-custom/package-lists/kali.list.chroot)
- [根据我的喜好附加包](https://github.com/prateepb/kali-live-build/blob/master/kali-config/variant-custom/package-lists/custom.list.chroot)（`kali-config/variant-custom/package-lists/custom.list.chroot`）
- [自定义脚本](https://github.com/prateepb/kali-live-build/blob/master/kali-config/variant-custom/hooks/live/customise.chroot)（`kali-config/variant-custom/hooks/live/customise.chroot`）
  - 克隆[我的个人点文件](https://github.com/prateepb/dotfiles)
  - [调整gnome菜单的大小](https://unix.stackexchange.com/questions/387843/how-can-i-resize-the-applications-menu-item-in-gnome)

我还在以下位置自定义了时区，键盘布局和语言环境：

- [`auto/config`](https://github.com/prateepb/kali-live-build/blob/master/auto/config)
- [`kali-config/variant-custom/hooks/live/persistence-menu.binary`](https://github.com/prateepb/kali-live-build/blob/master/kali-config/variant-custom/hooks/live/persistence-menu.binary)

## 构建

要构建和使用自定义变体：

```
cd kali-live-build
./build.sh --verbose --variant自定义
```

完成后，可以在以下位置找到生成的ISO映像： `images/kali-linux-custom-rolling-amd64.iso`

## 代理构建 

如果您可能要运行多个构建，那么通过缓存代理（例如[Squid）](http://www.squid-cache.org/)路由所有内容很有用。这将显着加快后续运行。

例子[squid.conf](https://github.com/prateepb/kali-live-build/blob/master/squid.conf)

```
cd kali-live-build
apt-get install squid
cp squid.conf /etc/squid/squid.conf
/etc/init.d/squid开始
```

现在，您可以`build.sh`使用`--apt-http-proxy`参数运行：

```
出口http_proxy = http：// localhost：3128 /
./build.sh --verbose --variant自定义-\
--apt-http-proxy = $ {http_proxy}
```

## 项目地址

- `https://github.com/prateepb/kali-live-build`