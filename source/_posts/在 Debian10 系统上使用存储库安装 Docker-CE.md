---
title: 在 Debian10 系统上使用存储库安装 Docker-CE
comments: true
tags: docker-ce
categories: linux
keywords: 'linux,docker-ce'
description: 存储库安装docker-ce
abbrlink: 75aa58a
---



在新主机上首次安装 Docker-CE 之前，需要设置 Docker存储库。之后，您可以从存储库安装和更新 Docker 。如果不是在新主机上首次安装 Docker-CE，为确保安装不会出错，可以执行卸载旧版本操作。

## 卸载旧版本

Docker 的旧版本被称为`docker`，`docker.io`或`docker-engine`。如果已安装，请卸载它们：

```
sudo apt-get remove docker docker-engine docker.io containerd runc
```

## 安装步骤

1. 更新 apt 包索引：

   ```
   sudo apt-get update
   ```

2. 安装软件包以允许 apt 通过HTTPS使用存储库：

   ```
   sudo apt-get install \
   	apt-transport-https \
   	ca-certificates \
   	curl \
   	gnupg2 \
   	software-properties-common
   ```

3. 添加Docker的官方GPG密钥：

   ```
   curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
   ```

4. 使用下行命令来设置稳定的存储库：

   ```
   echo "deb https://download.docker.com/linux/debian buster stable" > /etc/apt/sources.list
   ```

   buster是Debian10的发行版的名称。lsb_release -cs 命令可以返回Debian发行版的名称。

5. 安装Docker-CE：

   更新 apt 包索引并安装最新版本的 Docker-CE。

   ```
   sudo apt-get update
   sudo apt-get install docker-ce docker-ce-cli containerd.io
   ```

6. 验证是否正解安装Docker-CE:

   ```
   sudo docker run hello-world
   ```

   此命令下载测试图像并在容器中运行它。容器运行时，它会打印参考消息并退出。

