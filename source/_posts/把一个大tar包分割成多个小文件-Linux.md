---
title: 把一个大 tar 包分割成多个小文件-Linux
comments: true
tags: tar
categories: linux
keywords: 'tar,linux'
description: 分割tar包
abbrlink: f4c68735
---


## 本文内容:
- 把大文件分割成多个小文件
- 把分割成的小文件合并成大文件

## 示例:
创建一个大tar包:
```
tar -cvjf home1.tar.bz2 /home/snail/*
```
把tar包分成多个小文件:
```
split -b 40M home1.tar.bz2 "home.tar.bz2.part"
```
把多个小文件合并:
```
cat home.tar.bz2.parta* > home2.tar.bz2
```