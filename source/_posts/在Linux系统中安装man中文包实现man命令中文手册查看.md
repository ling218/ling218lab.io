---
title: 在 Linux 系统中安装 man 中文包实现 man 命令中文手册查看
comments: true
tags: man
categories: linux
keywords: 'linux,man'
description: 当安装的系统是英文版本Linux系统时，本文介绍的就非常有用了
abbrlink: d571e6ad
---



### 下载源代码：

```
# wget https://src.fedoraproject.org/repo/pkgs/man-pages-zh-CN/v1.5.2.tar.gz/1bbdc4f32272df0b95146518b27bf4be/v1.5.2.tar.gz
# wget  https://src.fedoraproject.org/repo/pkgs/man-pages-zh-CN/manpages-zh-1.5.1.tar.gz/13275fd039de8788b15151c896150bc4/manpages-zh-1.5.1.tar.gz
```

### 解压并安装：

```
# tar xf manpages-zh-1.5.1.tar.gz
# cd manpages-zh-1.5.1/
# ./configure --disable-zhtw --prefix=/usr/local/zhman
# make && make install
```

### 为了不抵消 **man** ,我们新建别名  **cman** 命令作为中文查询

```
# cd ~
# echo "alias cman='man -M /usr/local/zhman/share/man/zh_CN' " >>.bash_profile
# source .bash_profile
```

### 测试，使用中文 **cman** 查询命令

```
# cman find
```

