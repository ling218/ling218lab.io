---
title: Apt-get 安装软件包过程被强制中断后
comments: true
tags: apt-get
categories: linux
keywords: apt-get
abbrlink: b9cfb6eb
description:
---

apt-get 操作被强行中断后，会出现无法进行新的安装和删除的情况只是提示您使用apt-get -f install来解决问题，但实际上，你按它的提示操作却解决不了任何问题。

<!-- more -->

这时你需要这样做：

```
# sudo dpkg --configure -a
# sudo apt-get update
# sudo apt-get install -f
# sudo apt-get autoremove 
# sudo apt-get upgrade
```

如果还有一些有问题的软件包可以用下面这个命令干掉它：

```
# dpkg -r pack
```

`-r` 选项：移除软件包（保留配置）