---
title: Kali Linux 2019.2 （中文）安装后的配置
comments: true
tags: kali
categories: linux
keywords: kali linux
description: Kali linux 安装后的设置
abbrlink: 93b02f07
---



话不多说，直接上干货。

打开终端，接下来的操作大部分依赖终端操作。


## 1.添加软件源

```
cat << EOF >> /etc/apt/sources.list
# 中科大源
deb http://mirrors.ustc.edu.cn/kali kali-rolling main
deb-src http://mirrors.ustc.edu.cn/kali kali-rolling main

# 阿里源
deb http://mirrors.aliyun.com/kali kali-rolling main
deb-src http://mirrors.aliyun.com/kali kali-rolling main

# 官方源
deb http://http.kali.org/kali kali-rolling main
deb-src http://http.kali.org/kali kali-rolling main

EOF
```

## 2.更新系统

```
apt-get update 
apt-get upgrade -y
```

## 3.安装网易云音乐

```
mkdir down
cd down
wget http://d1.music.126.net/dmusic/netease-cloud-music_1.2.1_amd64_ubuntu_20190428.deb
```

```
dpkg -i netease-cloud-music_1.2.1_amd64_ubuntu_20190428.deb
```

## 4.安装中文输入法

```
apt-get install fcitx fcitx-table-wubi
reboot
fcitx-configtool
```

## 5.安装 Typora ( Markdown编辑器 )

```
wget https://typora.io/linux/typora_0.9.70_amd64.deb
dpkg -i typora_0.9.70_amd64.deb
```

> 不要尝试去更新 Typora ,最新的几个测试版都不支持 Kali Linux。

## 6.汉化 Kali Linux 自带的 FireFox 浏览器

```
apt-get -y install firefox-esr-l10n-zh-cn
```

打开 FireFox 浏览器，点击右上角的菜单 - Preferences - Languages - 添加Chinese/China [zh-cn] - OK - 重启浏览器

## 7.安装 shadowsocks 配置 proxychains 实现科学上网

### 7.1 安装 shadowsocks

```
pip install shadowsocks
```

### 7.2 配置 shadowsocks

```
nano /root/assconfig.json
```

内容如下：

```
{
      "server": "服务器ip地址",
      "server_port": 服务器端口,
      "password": "密码",
      "method": "加密方式",
      "local_address": "127.0.0.1",
      "local_port":1080,
      "timeout": 300
} 
```

### 7.3 Socks转HTTP

```
vim /etc/proxychains.conf
```

> 将其中 socks4 127.0.0.1 那一行换成 socks5 127.0.0.1 1080  （一般是最后一行）

### 7.4 编辑 openssl.py 文件

```
vim /usr/local/lib/python2.7/dist-packages/shadowsocks/crypto/openssl.py
```

> : set nu 可在 vim 显示行号

将第52行 libcrypto.EVP_CIPHER_CTX_cleanup.argtypes = (c_void_p,)

改为libcrypto.EVP_CIPHER_CTX_reset.argtypes = (c_void_p,)

再次搜索cleanup（全文件共2处，此处位于111行),

将libcrypto.EVP_CIPHER_CTX_cleanup(self._ctx)

改为libcrypto.EVP_CIPHER_CTX_reset(self._ctx)

也就是修改两个cleanup函数改为两个reset函数。

> : wq 保存并退出。 

### 7.5 测试

```
cp /usr/lib/proxychains3/proxyresolv /usr/bin/
```

```
proxyresolv www.google.com
```

> 如果出现 timeout ,首先检查 ss 设置开了没有，如果没有请打开。

```
sslocal -c assconfig.json
```

再次进行测试：

```
proxyresolv www.google.com
```

使用以下格式打开应用：

```
proxychains firefox
```

