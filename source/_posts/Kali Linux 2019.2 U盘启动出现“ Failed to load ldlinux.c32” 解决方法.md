---
title: Kali Linux 2019.2 U盘启动出现“ Failed to load ldlinux.c32” 解决方法
comments: true
tags: ldlinux
categories: other
keywords: ldlinux
abbrlink: bf66fd08
description:
---

我有一台 ThinkPad T400 电脑。想安装一个 Kali Linux 2019.2 系统，用 UltraISO 制作的启动盘，U盘启动时出现 “Failed to load ldlinux.c32” 。

<!--more-->




## 要求：

- 容量8G的U盘
- Kali linux 2019.2 镜像文件
- 时间和耐心
- UltraISO



### 出现的问题：

利用UltraISO制作了Kali linux 2019.2的U盘启动，开机F12键USB启动时出现。

```
Failed to load ldlinux.c32
Boot failed: please change disks and press a key to continue
```

### 解决问题过程：

1. 在网上查询了很多，进行了很多次尝试。

2. 换了其他软件制作U盘启动，失败。

3. 换了其它发行版最新的系统镜像，失败。

4. 安装 Ubuntu 12.04 LTS 系统镜像，成功。可以正常升级到最新版本(TLS)Ubuntu 18.04。

5. 猜测是不是制作启动时写入方式有问题，尝试了多个写入方式（USB-HDD、USB-HDD+、USB-ZIP），失败。

6. 最后尝试选择 RAW 写入方式，问题解决，U盘启动顺利启动。




