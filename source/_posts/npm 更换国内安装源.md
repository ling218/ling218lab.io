---
title: npm 更换国内安装源
comments: true
tags: npm
categories: other
keywords: npm
description: 加快npm安装速度
abbrlink: d1895efe
---

执行下列命令更换国内源：
```
npm config set registry https://registry.npm.taobao.org
npm config list
```
