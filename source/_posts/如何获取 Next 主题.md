---
title: 如何获取 Next 主题
comments: true
tags: next
categories: blog
keywords: 'next,hexo'
description: 能获取最新版本，指定版本Next主题
abbrlink: 87b9d5ae
---

## 下载最新release版本

这种方式将**仅提供最新的 release 版本**

```
mkdir themes/next
$ curl -s https://api.github.com/repos/theme-next/hexo-theme-next/releases/latest | grep tarball_url | cut -d '"' -f 4 | wget -i - -O- | tar -zx -C themes/next --strip-components=1
```

## 下载指定的release版本

在少数情况下将有所帮助，但这并非推荐方式。

```
git clone --branch v6.0.0 https://github.com/theme-next/hexo-theme-next themes/next
```

## 下载最新master分支

可能**不稳定**，但包含最新的特性。推荐进阶用户和开发者按此方式进行。

```
 git clone https://github.com/theme-next/hexo-theme-next themes/next
```

