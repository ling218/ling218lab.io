---
title: Ruby gem 使用手册
comments: true
tags: Ruby
categories: other
keywords: 'Ruby,gem'
description: gem 使用手册
abbrlink: 82c0e10f
---


从网络上搜集来的，分享给大家。

## 为什么用

> gem 是Ruby 的一个包管理器


## 查看版本

```
ruby -v #查看 ruby 版本
gem -v #查看 gem 版本
```


## 源的管理

```
#列出源
gem source -l

#添加源
gem source -a https://gems.ruby-china.www/
gem source -a https://mirrors.ustc.edu.cn/rubygems/

#删除源
gem source -r https://rubygems.org/

#删除所有源
gem source --clear-all

#更新源
gem source -u
```



## 包的管理

### 创建包

```
gem build jekyll.gemspec #把jekyll.gemspec编译成jekyll.gem
```



### 安装包

```
gem install -l jekyll.gem #安装jekyll,从本地
gem install jekyll #安装jekyll,从gem源
gem install jekyll #安装jekyll,从远程服务器
gem install jekyll -v 4.0.0 #安装jekyll指定版本
```



### 更新包

```
gem update --system #更新gem自身
gem update #更新已经安装的所有包
gem update jekyll #更新指定包
gem cleanup #清除所有包旧版本，保留最新版本
```



### 卸载包

```
gem uninstall jekyll #卸载jekyll
gem uninstall jekyll --version=[ver] #卸载指定版本jekyll
```



### 查看包

```
gem environment #查看安装环境
gem list --local #查看本机已经安装的所有包
gem contents jekyll #显示jekyll包中所包含的文件
gem dependency jekyll -v 4.0.0 #列出与jekyll相互依赖的包
gem query -n ''[0-9]'' --local #查找本地含有数字的包
gem search log --both #从本地和远程服务器上查找含有log字符串的包
gem search log --remoter 从远程服务器上查找含有log字符串的包
```

