---
title: 使用 Debian-Live 创建小型 Debain/Ubuntu LiveCD
comments: true
tags: live-build
categories: live-build
keywords: 'live-build,linux,debian'
description: 在很多情况下，您都希望从外部媒体启动操作系统并在服务器上执行一些维护。
abbrlink: c2eb39f
---


## 安装构建环境的应用程序

```
# apt install \
	live-build \
	debootstrap \
	xorriso \
	squashfs-tools \
	live-boot 
```



## 创建最小化UbuntuLiveCD

```
# mkdir ubuntu-livecd
# cd ubuntu-livecd
# lb config --mode "ubuntu"
# lb config -d "eoan"
# lb config -p minimal
# lb config --parent-distribution "eoan"
# lb config --parent-archive-areas "main universe"
# lb config --mirror-bootstrap "http://mirrors.ustc.edu.cn/ubuntu/"
# lb config --mirror-chroot "http://mirrors.ustc.edu.cn/ubuntu/"
# lb config --mirror-binary "http://mirrors.ustc.edu.cn/ubuntu/"
# lb build
```



## 创建DebianLiveCD

```
# mkidr debian-livecd
# cd debian-livecd
# lb config --mode "debian"
# lb config -d "stretch"
# lb config -p minimal
# lb config --parent-distribution "stretch"
# lb config --username future
# lb config --parent-archive-areas "main non-free contrib"
# lb config --mirror-bootstrap "http://mirrors.ustc.edu.cn/debian/"
# lb config --mirror-chroot "http://mirrors.ustc.edu.cn/debian/"
# lb config --mirror-binary "http://mirrors.ustc.edu.cn/debian/"
# lb config --mirror-chroot-security "http://mirrors.163.com/debian/"
# lb config --mirror-binary-security "http://mirrors.163.com/debian/"
# lb config --mirror-chroot-volatile "http://mirrors.163.com/debian/"
# lb config --mirror-debian-installer "http://mirrors.163.com/debian/"
```

为DebianLiveCD添加软件：

```
# cat << EOF > config/package-lists/custom.list.chroot
byobu
bzip2
ca-certificates
curl
dnsutils
dstat
ftp
isc-dhcp-client
less
lftp
lsb-release
lsof
lsscsi
net-tools
nmap
ntfs-3g
ntpdate
openssh-client
openssl
parted
patch
procps
psmisc
stress
tcpdump
tree
unzip
vim
wget
whois
zsh
EOF
```

## 开始构建DebainLiveCD

```
# lb build
```

