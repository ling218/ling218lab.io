---
title: 将自定义 Debian Live 安装到USB驱动器 (img格式)
comments: true
tags: live-build
categories: live-build
keywords: debian live build
description: 将自定义Debian Live安装到USB驱动器 (img格式)
abbrlink: 93f8863
---


这些说明**不是**用于实时安装，而是用于将Debian安装到USB闪存介质。

这里的一个额外好处是，我们将首先在`img`作为环回设备安装的文件中准备Debian chroot 。

然后，我们可以将该`img`文件用作通用基础映像，然后将其部署到大量USB驱动器中。

你为什么想做这个？好吧，一方面，该`img`文件有助于减少混乱，并为您提供易于管理的文件，该文件可以一次又一次地重新部署。这对于创建将在多台计算机上重用的通用映像也是理想的。就像您在网吧中所做的那样，每次启动时都会重新安装Debian，或者在教室中。

**警告**：我已**突出显示**您应该在chroot环境中的所有位置。小心！在本地环境而不是chroot中运行其中一些命令可能会导致问题。

```
sudo apt-get install \
    debootstrap
```

创建图像文件并将其格式化。

```
fallocate -l 2G debian.img
echo -e "o\nn\np\n1\n\n\nw" | sudo fdisk debian.img
echo -e "a\nw\n" | sudo fdisk debian.img
```

**请注意以下命令返回的内容。**对我来说，回送设备是`/dev/loop0`，但它可能因您而异。如果您的回送设备地址不同，请相应地更新以下说明。

```
sudo losetup --partscan --show --find debian.img
```

格式化回送设备的linux分区，就像我们对真实物理设备所做的一样。

```
sudo mkfs.ext4 /dev/loop0p1
```

挂载分区。

```
sudo mkdir -p /mnt/debian
sudo mount /dev/loop0p1 /mnt/debian
```

引导chroot。

```
sudo debootstrap \
    --arch=i386 \
    --variant=minbase \
    stretch /mnt/debian http://ftp.us.debian.org/debian/
```

将特殊设备安装到chroot。这对于以后安装grub非常重要。

```
sudo mount -t proc /proc /mnt/debian/proc
sudo mount -t sysfs /sys /mnt/debian/sys
sudo mount -o bind /dev /mnt/debian/dev
```

将root更改为chroot环境。

```
sudo chroot /mnt/debian
```

**chroot**

至少需要这些软件包，但根据需要添加更多。

**当提示您将grub安装到设备上时，请勿安装它。只需单击“ Enter”，然后选择不将其安装到任何设备。**

```
apt-get update && \
apt-get install --no-install-recommends \
    linux-image-586 systemd-sysv \
	grub2-common grub-pc
```

**chroot**

我们要基于磁盘标签而不是UUID或类似的命名磁盘`fstab`进行装载。这是因为，根据计算机的不同，USB驱动器*可能不是/ dev / sda*，而且因为UUID会根据文件的部署位置而有所不同。`/``/dev/sda``img`

```
echo "LABEL=DEBUSB / ext4 defaults 0 1" > /etc/fstab
```

**chroot**

```
passwd root
```

**chroot**

```
grub-install \
    --target=i386-pc \
    --boot-directory=/boot \
    --force-file-id \
    --skip-fs-probe \
    --recheck /dev/loop0
```

**chroot**

```
exit
```

在中编辑`grub.cfg`文件`img`。

```
sudo nano /mnt/debian/boot/grub/grub.cfg
```

将此内容粘贴到`grub.cfg`文件中。

```
# grub.cfg
set default="0"
set timeout=10

menuentry "Debian" {
    linux /vmlinuz root=/dev/disk/by-label/DEBUSB quiet
    initrd /initrd.img
}
```

使用与我们`fstab`上面相同的名称标记映像分区。

```
sudo e2label /dev/loop0p1 DEBUSB
```

设置的主机名`img`。

```
echo "debian-usb" | sudo tee /mnt/debian/etc/hostname
```

清理特殊设备。

```
sudo umount /mnt/debian/{dev,sys,proc}
```

卸载回路设备。

```
sudo umount /mnt/debian
```

卸下`img`。

```
sudo losetup -d /dev/loop0
```

现在，您具有通用的Debian安装，可以像这样将其部署到多个设备。

```
dd if=debian.img of=/dev/sdz
```

# 引文

- [在USB密钥上安装Arch Linux](https://wiki.archlinux.org/index.php/Installing_Arch_Linux_on_a_USB_key)
- [从USB 3启动](http://www.wyae.de/docs/boot-usb3/)
- [标记Linux分区](https://www.cyberciti.biz/faq/linux-partition-howto-set-labels/)
- [如何生成使用LABEL而不是UUID的grub.cfg？](https://ubuntuforums.org/showthread.php?t=1529777)
- [格鲁布](https://wiki.archlinux.org/index.php/GRUB)
- [6.3多引导手动配置](https://www.gnu.org/software/grub/manual/html_node/Multi_002dboot-manual-config.html)
- [可以安全删除/ boot中的System.map- *文件吗？](https://unix.stackexchange.com/questions/10010/safe-to-delete-system-map-files-in-boot)
- [使用标签引导GRUB2（无UUID）](https://ubuntuforums.org/showthread.php?t=1530532)
- [16.3.64搜索](https://www.gnu.org/software/grub/manual/grub.html#search)
- [错误：没有这样的设备：grub抢救>](https://ubuntuforums.org/showthread.php?t=1854142)
- [grub救援->没有这样的分区[重复\]](https://askubuntu.com/questions/491604/grub-rescue-no-such-partition)
- [如何将grub安装到.img文件中？](https://superuser.com/questions/130955/how-to-install-grub-into-an-img-file)
- [如何解决引导进入initramfs提示符和“装载：无法读取'/ etc / fstab'：无此文件或目录”和“找不到init”的问题？](https://unix.stackexchange.com/questions/120198/how-to-fix-boot-into-initramfs-prompt-and-mount-cant-read-etc-fstab-no-su)
- [`sudo echo “bla” >> /etc/sysctl.conf` 没有权限](https://serverfault.com/questions/540492/sudo-echo-bla-etc-sysctl-conf-permission-denied)
- [在chroot环境中挂载dev，proc，sys？](https://superuser.com/questions/165116/mount-dev-proc-sys-in-a-chroot-environment)

## 原文地址：

- https://willhaley.com/blog/install-debian-usb/