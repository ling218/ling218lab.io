---
title: Hexo 使用 Next7.6 主题搭建博客详细记录
date: '2019/12/3 8:00:00'
comments: true
tags: next7.6
categories: blog
keywords: 'hexo,next7.6'
abbrlink: 2439a4f6
description:
---

Hexo 是一个快速、简洁且高效的博客框架，Next 主题是 Hexo 一个漂亮的主题，但是个性化 Next 主题时发现网上大部分教程都是 Next7 版本以下的设置教程，本文特记录 Next7 主题设置内容方法。

<!--more-->



## 主题设置

### 设置站点语言

编辑**站点配置文件**`_config.yml`，将的值设置 `language` 为所需的语言。

```
language: zh-CN
```

Next 更新至 Next 7 版本以后，支持语言代码发生改变，[具体请参阅](https://theme-next.org/docs/getting-started/)。

| 语言                     | 中文          | 代码  |
| :----------------------- | :------------ | :---- |
| 🇨🇳 Chinese (Simplified)  | 简体中文      | zh-CN |
| 🇨🇳 Chinese (Traditional) | 繁體中文      | zh-TW |
| 🇨🇳 Chinese (Hong Kong)   | 繁體中文-香港 | zh-HK |
| 🇺🇸 English               | 英语          | en    |

### 设置菜单

1、默认情况下，NexT提供 home 和提供 archives。启用 about、tags 、categories 菜单时，要在**主题配置文件**`themes/next/_config.yml` 中取消前面的注释。同时您还可以自定义自已的页面。

```
menu:  
home: / || home                        #首页  
#about: /about/ || user                #关于我们  
#tags: /tags/ || tags                  #标签  
#categories: /categories/ || th        #分类  
archives: /archives/ || archive        #归档  
#schedule: /schedule/ || calendar      #日历  
#sitemap: /sitemap.xml || sitemap      #网站地图  
#commonweal: /404/ || heartbeat        #公益404页面
```

2、同时 Next7 支持层次结构内的动态子菜单，请在**主题配置文件**`themes/next/_config.yml` 中添加您的子菜单设置。

```
menu:
  home: / || home
  archives: /archives/ || archive
  Docs:
    default: /docs/ || book
    Third Party Services:
      default: /third-party-services/ || plug
      Algolia Search: /algolia-search/ || adn
```

### 设置头像

NexT 在侧边栏中显示头像。在**主题配置文件**`themes/next/_config.yml` 中编辑`avatar`值来配置。

```
avatar:
  url: /images/avatar.gif         #请将头像文件上传至/them/source/images文件中
  rounded: true                   #设置头像为圆形
  rotated: true                   #设置头像在鼠标悬停时旋转
```

### 设置作者署名

在**主题配置文件**`themes/next/_config.yml` 中编辑`author`值来配置。

```
# Site
author: ling218
```

### 设置边栏RSS支持

在**主题配置文件**`themes/next/_config.yml` 中编辑`creative_commons`部分来配置。

```
npm install --save hexo-generator-feed
```

### 设置Creative Commons 4.0国际许可

在**主题配置文件**`themes/next/_config.yml` 中编辑`creative_commons`部分来配置。

```
creative_commons:
  license: by-nc-sa 
  sidebar: true 
  post: true 
  language: deed.zh
```

### 设置页面阅读进度条

在**主题配置文件**`themes/next/_config.yml` 中将`reading_progress.enable`的值设置`true`启用。

```
# Reading progress bar
reading_progress:
  enable: true
  # Available values: top | bottom 显示位置
  position: top
  color: "#37c6c0"
  height: 2px
```

### 设置网站开始时间

在**主题配置文件**`themes/next/_config.yml` 中的`since`部分编辑。

```
footer:  since: 2019
```

### 设置取消网站平台信息

默认情况下，Powered by Hexo v4.1.0 | Theme — NexT.Muse v7.6.0 会显示在网站下面。通过修改**主题配置文件**`themes/next/_config.yml` 中`powered`和`theme`部分中的值进行配置。

```
powered:
  # Hexo link (Powered by Hexo).
  enable: false
  # Version info of Hexo after Hexo link (vX.X.X).
  version: true

theme:
  # Theme & scheme info link (Theme - NexT.scheme).
  enable: false
  # Version info of NexT after scheme info (vX.X.X).
  version: true
```

### 设置网站备案信息

在**主题配置文件**`themes/next/_config.yml` 中的`beian`部分编辑。

```
  beian:
    enable: true
    icp: 京ICP备 1234567890号-1
    gongan_id: 1234567890
    gongan_num: 京公网安备 1234567890号
    gongan_icon_url: /uploads/beian.png
```

### 设置本地搜索

地搜索不需要任何外部第三方服务，但是需要依赖`hexo-generator-searchdb`，安装完`hexo-generator-searchdb`需要设置**站点配置文件**`_config.yml`和**主题配置文件**`themes/next/_config.yml` 。

```
npm install hexo-generator-searchdb --save
```

1、**站点配置文件**`_config.yml`设置，添加如下设置。

```
search:  
  path: search.xml  
  field: post  
  format: html  
  limit: 10000
```

2、**主题配置文件**`themes/next/_config.yml` 设置，修改成如下设置。

```
# Local search
local_search:  
  enable: true  
  trigger: auto  
  top_n_per_article: 1   
  unescape: false  
  preload: false
```

### 设置发布字数统计

发布字数统计依赖于`hexo-symbols-count-time`，在站点根目录中运行以下命令来安装：

```
npm install hexo-symbols-count-time --save
```

1、编辑**站点配置文件**`_config.yml`，如果**配置文件**中没有`symbol_count_time`，请添加下面内容。

```
symbols_count_time:
  symbols: true
  time: true
  total_symbols: true
  total_time: true
  exclude_codeblock: true
```

2、在**主题配置文件**`themes/next/_config.yml` 中的`symbols_count_time`部分编辑。

```
# Dependencies: https://github.com/theme-next/hexo-symbols-count-time
symbols_count_time:
  separated_meta: true
  item_text_post: true
  item_text_total: true
  awl: 4
  wpm: 275
  suffix: mins.
```

### 设置文章发布显示

NexT 支持帖子创建日期，帖子更新日期和帖子类别显示。通过**主题配置文件**`themes/next/_config.yml` 中的`post_meat`部分编辑。

```
# Post meta display settings
post_meta:
  item_text: true
  created_at: true
  updated_at:
    enable: true
    another_day: true
  categories: true
```

### 设置侧边栏样式

默认情况下，侧边栏仅显示在帖子中（具有目录），并位于左侧。在**主题配置文件**`themes/next/_config.yml` 中`sidebar`部分中，编辑来更改它。

```
sidebar:
  # 侧边栏显示的位置.
  #position: left
  position: right

  # Manual define the sidebar width. If commented, will be default for:
  # Muse | Mist: 320
  # Pisces | Gemini: 240
  #width: 300

  # 侧边栏显示的方式:
  #  - post    仅在具有索引的帖子中显示侧边栏.
  #  - always  在所有页面中显示侧边栏.
  #  - hide    将其隐藏在所有页面中（但可以由用户手动打开）.
  #  - remove  完全移除侧边栏.
  display: post
  
  # Sidebar offset from top menubar in pixels (only for Pisces | Gemini).
  offset: 12

  # 移动设备显示侧边栏. (only for Muse | Mist).
  onmobile: true
```

### 设置侧边栏显示状态

通过编辑**主题配置文件**`themes/next/_config.yml` 中`site_state`部分中的值，来配置侧边栏是否显示帖子/类别/标签的类别和计数。

```
# Posts / Categories / Tags in sidebar.
site_state: true
```

### 设置侧边栏社交链接

编辑**主题配置文件**`themes/next/_config.yml` 中的`social`部分，启用GitHub、E-Mail、Weibo、Google和Skype如下所示：

```
# Social Links
# Usage: `Key: permalink || icon`
# Key is the link label showing to end users.
# Value before `||` delimiter is the target permalink.
# Value after `||` delimiter is the name of Font Awesome icon. If icon (with or without delimiter) is not specified, globe icon will be loaded.
social:
  GitHub: https://github.com/hnleyan || github
  E-Mail: mailto:hn.zmd.ly@gmail.com || envelope
  Weibo: https://weibo.com/u/5690148064 || weibo
  Google: https://plus.google.com/hn.zmd.ly || google
  #Twitter: https://twitter.com/yourname || twitter
  #FB Page: https://www.facebook.com/yourname || facebook
  #VK Group: https://vk.com/yourname || vk
  #StackOverflow: https://stackoverflow.com/yourname || stack-overflow
  #YouTube: https://youtube.com/yourname || youtube
  #Instagram: https://instagram.com/yourname || instagram
  Skype: skype:1075180052?call|chat || skype
social_icons:
  enable: true
  icons_only: false
  transition: false
```

### 设置侧边栏回到顶部

将**主题配置文件**`themes/next/_config.yml` 中的`back2top`设置即可，侧边栏回到顶部设置如下所示：

```
back2top:
  enable: true
  # Back to top in sidebar.
  sidebar: false
  # Scroll percent label in b2t button.
  scrollpercent: true
```

### XXXXX设置主页文章摘要

默认情况下，主页上文章显示全部。通过设置**主题配置文件**`themes/next/_config.yml` 中的`excerpt_description`和`auto_excerpt`。如下所示：

```
# Automatically excerpt description in homepage as preamble text.
excerpt_description: true

# Automatically excerpt (Not recommend).
# Use <!-- more --> in the post to control excerpt accurately.
auto_excerpt:
  enable: true
  length: 150

# Read more button
# If true, the read more button would be displayed in excerpt section.
read_more_btn: true
```

### 设置页面分类

Next 主题以前不支持同篇文章多个同级分类，Next7 更改以前设置，支持同级分类。修改`blog_dir/source/_posts/`中文章的`categories`即可。
1、文章多个同级分类写法，如下所示：

```
categories: 
 - [python] 
 - [pycharme] 
 - [Anaconda3]
```

2、文章父子级分类写法，如下所示：

```
categories: 
 - python 
 - pycharme 
 - Anaconda
```

### 设置标签云

通过设置**主题配置文件**`themes/next/_config.yml` 中的`tagcloud`。设置标签云最大和最小一样大，如下所示：

```
tagcloud:
  # All values below are same as default, change them by yourself
  min: 22 # min font size in px
  max: 22 # max font size in px
  start: '#ccc' # start color (hex, rgba, hsla or color keywords)
  end: '#111' # end color (hex, rgba, hsla or color keywords)
  amount: 200 # amount of tags, chage it if you have more than 200 tags

```

### 设置主题背景

1、3D式动态背景设置，修改如下所示：

```
# JavaScript 3D library.
# Dependencies: https://github.com/theme-next/theme-next-three
three:
  enable: true
  three_waves: false
  canvas_lines: true
  canvas_sphere: false
  ......
  
# Internal version: 1.0.0
# Example:
# three: //cdn.jsdelivr.net/gh/theme-next/theme-next-three@1/three.min.js
# three_waves: //cdn.jsdelivr.net/gh/theme-next/theme-next-three@1/three-waves.min.js
# canvas_lines: //cdn.jsdelivr.net/gh/theme-next/theme-next-three@1/canvas_lines.min.js
# canvas_sphere: //cdn.jsdelivr.net/gh/theme-next/theme-next-three@1/canvas_sphere.min.js
three: //cdn.jsdelivr.net/gh/theme-next/theme-next-three@1/three.min.js
three_waves: //cdn.jsdelivr.net/gh/theme-next/theme-next-three@latest/three-waves.min.js
canvas_lines: //cdn.jsdelivr.net/gh/theme-next/theme-next-three@latest/canvas_lines.min.js
canvas_sphere: //cdn.jsdelivr.net/gh/theme-next/theme-next-three@latest/canvas_sphere.min.js
```

2、线条式背景设置。

```
# Canvas-nest
# Dependencies: https://github.com/theme-next/theme-next-canvas-nest
# For more information: https://github.com/hustcc/canvas-nest.js
canvas_nest:
  enable: false
  onmobile: true # Display on mobile or not
  color: "0,0,0" # RGB values, use `,` to separate
  opacity: 0.5 # The opacity of line: 0~1
  zIndex: -1 # z-index property of the background
  count: 99 # The number of lines

# Canvas-ribbon
# Dependencies: https://github.com/theme-next/theme-next-canvas-ribbon
# For more information: https://github.com/zproo/canvas-ribbon
canvas_ribbon:
  enable: false
  size: 300 # The width of the ribbon
  alpha: 0.6 # The transparency of the ribbon
  zIndex: -1 # The display level of the ribbon
  
......

  # Internal version: 1.0.0
  # Example:
  #canvas_nest: //cdn.jsdelivr.net/gh/theme-next/theme-next-canvas-nest@1/canvas-nest.min.js
  #canvas_nest_nomobile: //cdn.jsdelivr.net/gh/theme-next/theme-next-canvas-nest@1/canvas-nest-nomobile.min.js
  canvas_nest: //cdn.jsdelivr.net/gh/theme-next/theme-next-canvas-nest@latest/canvas-nest.min.js
  canvas_nest_nomobile: //cdn.jsdelivr.net/gh/theme-next/theme-next-canvas-nest@latest/canvas-nest-nomobile.min.js
```

### 主题设置注意事项

主题设置完，生成发布无效的，可以通过删除缓存，在站点根目录中生成发布，如下所示：

```
hexo clean
hexo d -g
```

### 图片灯箱

添加灯箱功能，实现点击图片后放大聚焦图片，并支持幻灯片播放、全屏播放、缩略图、快速分享到社交媒体等，该功能由 [fancyBox](https://github.com/fancyapps/fancybox) 提供。

在根目录下执行以下命令安装相关依赖：

```
$ git clone https://github.com/theme-next/theme-next-fancybox3 themes/next/source/lib/fancybox
```

通过设置**主题配置文件**`themes/next/_config.yml` 中的 `fancybox`。

```
fancybox: true
```

### 分类时间线

归档页面的时间线会让文章显示得很有条理，但是分类里却没有，可以通过修改布局自己实现这个时间线功能。

在主题的分类布局文件中添加以下代码：

```
	{% for post in page.posts %}
+		{# Show year #}
+			{% set year %}
+			{% set post.year = date(post.date, 'YYYY') %}
+			{% if post.year !== year %}
+			{% set year = post.year %}
+			<div class="collection-title">
+			<h2 class="archive-year motion-element" id="archive-year-{{ year }}">{{ year }}</h2>
+			</div>
+			{% endif %}
+		{# endshow #}
		{{ post_template.render(post) }}
	{% endfor %}
	
	……
	
{% block sidebar %}
  {{ sidebar_template.render(false) }}
{% endblock %}

+{% block script_extra %}
+  {% if theme.use_motion %}
+		<script type="text/javascript" id="motion.page.archive">
+			$('.archive-year').velocity('transition.slideLeftIn');
+		</script>
+	{% endif %}
+{% endblock %}
```

### 文章推荐

该功能由 [hexo-related-popular-posts](https://github.com/tea3/hexo-related-popular-posts) 插件提供，效果如图：

在站点根目录中执行以下命令安装依赖：

```
$ npm install hexo-related-popular-posts --save
```

在 **主题配置文件** _config.yml 中开启相关文章推荐功能：

```
related_posts:
  enable: true
  title:  # custom header, leave empty to use the default one
  display_in_home: false
  params:
    maxCount: 3
```

此时会在每篇文章结尾根据标签相关性和内容相关性来推荐相关文章。

事实上并非每篇文章都需要开启该功能，可在文章 Front-Matter 中设置 `related_posts` 字段来控制是否在文末显示相关文章，然后修改文章布局模板中相关的判定条件：

```
- {% if theme.related_posts.enable and (theme.related_posts.display_in_home or not is_index) %}
+ {% if theme.related_posts.enable and (theme.related_posts.display_in_home or not is_index) and post.related_posts %}
    {% include 'post-related.swig' with { post: post } %}
  {% endif %}
```

为了方便可在草稿模板 scaffolds\draft.md 中统一添加 `related_posts` 字段默认值：

```
  title: {{ title }}
  tags:
  categories:
+ related_posts: true
```

### Valine评论

首先，在LeanCloud上注册账号并创建应用，设置LeanCloud的信息。

在 **存储** -> **数据** 中 新建一个名为`Counter` 的 Class，`ACL`权限设置为 **无限制**：
在 **设置** -> **安全中心** 中添加博客域名到 Web 安全域名中，以保护LeanCloud应用的数据安全。

然后，在 **主题配置文件** _config.yml 开启评论功能即可：

```
valine:
  enable: true    # 开启 Valine 评论
  # 设置应用 id 和 key
  appid:  # your leancloud application appid
  appkey:  # your leancloud application appkey
  # 关闭提醒与验证
  notify: false
  verify: false
  placeholder:  # 文本框默认文字
  avatar: mm  # gravatar style
  guest_info: nick,mail # 需要填写的信息字段
  pageSize: 10  # 每页评论数
  language: zh-cn # language, available values: en, zh-cn
  visitor: true # 开启文章阅读次数统计
  comment_count: false # 首页是否开启评论数
```

最后，集成评论服务后，所有的页面也会带有评论，包括标签、关于等页面。这里需要在添加字段`comments`并将值设置为 false 即可。

```
---
title: 标签
type: "tags"
comments: false
---
```

