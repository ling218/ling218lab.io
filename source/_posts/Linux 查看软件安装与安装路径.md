---
title: Linux 查看软件安装与安装路径
comments: true
tags: linux
categories: linux
keywords: linux
abbrlink: 46bd345b
description: 整理记录
---


## 查看软件是否安装
以deb包安装的，可以用dpkg -l能看到。如果是查找指定软件包，用dpkg -l | grep “软件或者包的名字”：
`dpkg -l | grep xyz`

以yum方法安装的，可以用yum list installed查找，如果是查找指定包，命令后加 | grep “软件名或者包名：
`yum list xyz`

以rpm包安装的，可以用rpm -qa看到，如果要查找某软件包是否安装，用 rpm -qa | grep “软件或者包的名字”:
`rpm -qa | grep xyz`

## 查看软件路径
`whereis nginx`

- /usr/sbin/naginx ---执行路径
- /usr/lib64/nginx ---安装路径
- /etc/nginx/ --- 配置路径
- /usr/share/nginx ---默认站点目录