---
title: Shell 脚本同时执行多条命令
comments: true
tags: shell
categories: linux
keywords: 'shell,frp'
description: 把启动两个frpc的命令封装成一条命令，使用shell脚本完成
abbrlink: 626682b4
---



在利用frp穿透内网的过程中，由于需要启动两个frpc进程，在利用`Systemd Service`实现开机自启的过程中，由于[Service] 部分`Type为simple时`，
`ExecStart`后面只能跟一条命令，因此需要把启动两个frpc的命令封装成一条命令，使用shell脚本完成。

在`/opt/frpc/`目录下新建一个`frpc.sh`，并赋予执行权限

```
cd /opt/frpc/
touch frpc.sh && chmod +x frpc.sh
vim frpc.sh
```

其中frpc.sh的内容为

```
#!/bin/bash

/opt/frpc/frpc_80/./frpc -c /opt/frpc/frpc_80/./frpc.ini &

/opt/frpc/frpc_1638/./frpc -c /opt/frpc/frpc_1638/./frpc.ini
```

注意：

- `&` 表示几条命令同时执行；
- `&&`表示命令执行具有先后顺序，前一条命令执行成功后，才执行下一条命令。
- `||`表示前一条命令执行成功后，后一条命令就不会执行。