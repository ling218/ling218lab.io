---
title: Linux 创建 sudo 用户
comments: true
tags: sudo
categories: linux
keywords: 'linux,sudo'
abbrlink: 2a8b1abe
description:
---

`sudo` 命令提供了临时使用root权限的机制，使普通用户可以执行超级管理员任务。

<!--more-->



添加用户。

```
# adduser newuser
```

设置用户密码。

```
# passwd newuser
```

把新建用户加入sudo组。

```
# usermod -aG sudo newuser
```

测试新用户sudo机制。

```
# su newuser
# sudo apt update
```

